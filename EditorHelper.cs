﻿using System.Diagnostics;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SMDev.Unitilities
{
    public static class EditorHelper
    {
        [Conditional("UNITY_EDITOR")]
        public static void SaveAsset(Object asset)
        {
#if UNITY_EDITOR
            EditorUtils.SaveAsset(asset);
#endif
        }

        [Conditional("UNITY_EDITOR")]
        public static void SetDirty(Object asset)
        {
#if UNITY_EDITOR
            EditorUtility.SetDirty(asset);
#endif
        }

        [Conditional("UNITY_EDITOR")]
        public static void SaveAssets()
        {
#if UNITY_EDITOR
            AssetDatabase.SaveAssets();
#endif
        }
    }
}
