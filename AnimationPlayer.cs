﻿using UnityEngine;

namespace SMDev.Unitilities
{
    public class AnimationPlayerSO : AnimationPlayer
    {
        AnimationPlayerState state;

        [EditInline]
        public AnimationPlayerScriptableObject ScriptableObject;
        public override void Play(Animator animator)
        {
            if (ScriptableObject != null)
            {
                ScriptableObject.Settings.PlayInternal(animator, state);
            }
        }

        public override void Update(Animator animator, float deltaTime)
        {
            if(ScriptableObject != null)
            {
                ScriptableObject.Settings.UpdateInternal(animator, deltaTime, state);
            }
        }
    }
    internal class AnimationPlayerState
    {
        public float timeSinceLastPlayStarted = 0;
        public bool isPlaying = false;
    }


    [System.Serializable]
    public abstract class AnimationPlayer
    {
        
        AnimationPlayerState state;
        public float TimeSinceLastPlayStarted => state.timeSinceLastPlayStarted;
        public bool IsPlaying => state.isPlaying;
        public virtual void Play(Animator animator)
        {
            PlayInternal(animator, state);
        }
        public virtual void Update(Animator animator, float deltaTime)
        {
            UpdateInternal(animator, deltaTime, state);
        }
        internal virtual void PlayInternal(Animator animator, AnimationPlayerState state)
        {
            state.timeSinceLastPlayStarted = 0;
        }

        internal virtual void UpdateInternal(Animator animator, float deltaTime, AnimationPlayerState state)
        {
            state.timeSinceLastPlayStarted += deltaTime;
        }
    }

    public static class AnimationPlayerExtensions
    {
        public static void TryPlay(this AnimationPlayer player, Animator animator)
        {
            if (animator != null && player != null)
            {
                player.Play(animator);
            }
        }

        public static void TryUpdate(this AnimationPlayer player, Animator animator, float deltaTime)
        {
            if (animator != null && player != null)
            {
                player.Update(animator, deltaTime);
            }
        }
    }
    public class AnimationBoolParameterPlayer : AnimationPlayer
    {
        public string BoolParameter;

        public float Duration;

        public override void Play(Animator animator)
        {
            base.Play(animator);
            animator.SetBool(BoolParameter, true);
        }

        public override void Update(Animator animator, float deltaTime)
        {
            bool isFinished = TimeSinceLastPlayStarted >= Duration;
            base.Update(animator, deltaTime);
            if (!isFinished && TimeSinceLastPlayStarted >= Duration)
            {
                animator.SetBool(BoolParameter, false);
            }
        }
    }
    public class AnimationStatePlayer : AnimationPlayer
    {
        public int layer = -1;
        public string animatorPlayState;

        public override void Play(Animator animator)
        {
            base.Play(animator);
            animator.Play(animatorPlayState, layer);
        }

        bool CheckPlayingOnLayer(Animator animator, int layer)
        {
            return animator.GetCurrentAnimatorStateInfo(layer).IsName(animatorPlayState);
        }

        bool CheckPlaying(Animator animator)
        {
            if (layer < 0)
            {
                for (int i = 0; i < animator.layerCount; i++)
                {
                    if (CheckPlayingOnLayer(animator, layer))
                    {
                        return true;
                    }
                }
                return false;
            }
            return CheckPlayingOnLayer(animator, layer);
        }

        internal override void UpdateInternal(Animator animator, float deltaTime, AnimationPlayerState state)
        {
            base.UpdateInternal(animator, deltaTime, state);
            state.isPlaying = CheckPlaying(animator);
        }
    }

}