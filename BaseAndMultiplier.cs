﻿using System;

namespace SMDev.Unitilities
{

    [Serializable]

    public class BaseAndMultiplier
    {
        public float baseValue = 0;
        public float multiplier = 1;

        public float Value => baseValue * multiplier;

        public static implicit operator float(BaseAndMultiplier s) => s.Value;

        public float ModifyBase(float baseValue) => (baseValue + this.baseValue) * multiplier;

        public override string ToString()
        {
            return Value.ToString();
        }
    }
}