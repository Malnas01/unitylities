﻿using UnityEngine;

namespace SMDev.Unitilities
{
    public class ScriptableObjectListItem : ScriptableObject, IIndexable
    {
        [UpdateObjectName]
        public string Name;

        [SerializeField]
        int id;

        public int Index { get => id; set => id = value; }
    }

    public interface IIndexable
    {
        public int Index { get; set; }
    }
}
