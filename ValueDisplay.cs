﻿using TMPro;

namespace SMDev.Unitilities
{
    [System.Serializable]
    public class ValueDisplay
    {
        public TMP_Text Label;
        public string FormatString = "{0}";

        public void Display(params object[] args)
        {
            if (Label != null)
            {
                Label.text = string.Format(FormatString, args);
            }
        }

        public ValueDisplay(string formatString)
        {
            FormatString = formatString;
        }

    }
}