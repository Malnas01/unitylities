﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Object = UnityEngine.Object;

namespace SMDev.Unitilities
{
    public class CustomLookupTableAttribute : Attribute
    {
        public readonly Type CustomLookupTableType;
        public CustomLookupTableAttribute(Type CustomType)
        {
            if(!typeof(ObjectLookupTable).IsAssignableFrom(CustomType))
            {
                throw new ArgumentException($"{CustomType} is not an instance of {typeof(ObjectLookupTable)}");
            }
            CustomLookupTableType = CustomType;
        }
    }
    public class ObjectLookupTable : ScriptableObject
    {
        internal const string POSTFIX = "_Lookup";
        internal const string RESOURCES_PATH = "Lookup Tables/";
        internal const string ASSET_PATH = "Assets/Resources/" + RESOURCES_PATH;
        public static string GetResourcesLoadPath(Type type) => Path.Combine(RESOURCES_PATH, GetName(type));
        public static string GetName(Type type) => type.Name + POSTFIX;

        static Dictionary<Type, ObjectLookupTable> lookupTables = new Dictionary<Type, ObjectLookupTable>();
        static Dictionary<Type, ObjectLookupTable> childLookupTables = new Dictionary<Type, ObjectLookupTable>();

        [SerializeField, EditInline, NonReorderable, ReadOnlyInspector]
        internal List<Object> list = new List<Object>();

        [ReadOnlyInspector] public string className;

        public static T GetObject<T>(int index) where T : Object, IObjectWithID
        {
            return GetLookup<T>(true)[index] as T;
        }

        public event Action<IObjectWithID> OnObjectAdded;
        public event Action<IObjectWithID> OnObjectIdChanged;

        public static ObjectLookupTable GetLookup<T>(bool searchParentClass = true) => GetLookup(typeof(T), searchParentClass);
        public static ObjectLookupTable GetLookup(Type type, bool searchParentClass = true)
        {
            ObjectLookupTable lookup;
            //Debug.Log("Retrieving lookup for " + typeof(T));
            if (lookupTables.TryGetValue(type, out lookup))
            {
                if(lookup != null)
                {
                    return lookup;
                } 
                else
                {
                    lookupTables.Remove(type);
                }
            }

            if (searchParentClass && childLookupTables.TryGetValue(type, out lookup))
            {
                if (lookup != null)
                {
                    return lookup;
                }
                else
                {
                    childLookupTables.Remove(type);
                }
            }

            var interfaceType = typeof(IObjectWithID);
            if (!interfaceType.IsAssignableFrom(type))
            {
                return null;
            }

            lookup = Resources.Load<ObjectLookupTable>(GetResourcesLoadPath(type));

            if(lookup == null && searchParentClass)
            {
                var baseType = type.BaseType;
                while(baseType != null)
                {
                    if (baseType.IsGenericType && baseType.GetGenericTypeDefinition() == typeof(ScriptableObjectWithID<>))
                    {
                        lookup = GetLookup(baseType.GetGenericArguments()[0], false);
                        if (lookup != null)
                        {
                            childLookupTables.Add(type, lookup);
                            return lookup;
                        }
                    }
                    if(interfaceType.IsAssignableFrom(baseType) && !interfaceType.IsAssignableFrom(baseType.BaseType))
                    {
                        lookup = GetLookup(baseType, false);
                        if (lookup != null)
                        {
                            childLookupTables.Add(type, lookup);
                            return lookup;
                        }
                        break;
                    }
                    baseType = baseType.BaseType;
                }
            }

            //Debug.Log("Loading from " + GetResourcesLoadPath(type));
            if (lookup == null)
            {

                //Debug.Log("Not found. Creating.");
#if UNITY_EDITOR
                var attribute = Attribute.GetCustomAttribute(type, typeof(CustomLookupTableAttribute)) as CustomLookupTableAttribute;
                if (attribute != null)
                {
                    lookup = CreateInstance(attribute.CustomLookupTableType) as ObjectLookupTable;
                }
                else
                {
                    lookup = CreateInstance<ObjectLookupTable>();
                }

                EditorUtils.EnsureAssetPathExists(ASSET_PATH);
                UnityEditor.AssetDatabase.CreateAsset(lookup, Path.Combine(ASSET_PATH, GetName(type) + ".asset"));

#else
                Debug.LogError($"Unable to load lookup table for {type}");
#endif
            }

            lookupTables.TryAdd(type, lookup);
            lookup.className = type.FullName;
            return lookup;
        }

        public virtual void ClearAtIndex(int index)
        {
            list[index] = null;
        }

        public virtual void NotifyIDChange(IObjectWithID scriptableObjectWithID)
        {
            OnObjectIdChanged?.Invoke(scriptableObjectWithID);
        }
        public string GetTypeName()
        {
            var lookupIndex = name.IndexOf(POSTFIX);
            if (lookupIndex < 0)
            {
                Debug.LogError("Lookup table has an incorrectly set up name");
                return null;
            }
            return name.Remove(lookupIndex);
        }

        public Type GetElementType()
        {
            foreach(var a in AppDomain.CurrentDomain.GetAssemblies())
            {
                var type = a.GetType(className);
                if(type != null)
                {
                    return type;
                }
            }
            throw new TypeLoadException("Couldn't find type " + className);
        }
        public IObjectWithID this[int index]
        {
            get
            {
                if (index < 0) return null;
                if (index >= list.Count) return null;
                return list[index] as IObjectWithID;
            }
        }

        public virtual bool ValidateEntry(Object uObject)
        {
            if(uObject is not IObjectWithID so || so.UnityObject != uObject)
            {
                return false;
            }

            if (this[so.Id] == so) return false;

            if (so.Id >= 0)
            {
                while (list.Count <= so.Id) list.Add(null);
                if(list[so.Id] == null)
                {
                    list[so.Id] = so.UnityObject;
                    Debug.Log($"Added {so.UnityObject.name} to the lookup table {name} at it's assigned ID {so.Id}", so.UnityObject);
                    EditorHelper.SetDirty(this);
                    EditorHelper.SetDirty(so.UnityObject);
                    EditorHelper.SaveAssets();
                    OnObjectAdded?.Invoke(so);
                    return true;
                }
            }

            int freeIndex = list.Count;
            for(int i = 0; i < list.Count; i++)
            {
                if(i < freeIndex && list[i] == null)
                {
                    freeIndex = i;
                }
                else if (list[i] == so.UnityObject)
                {
                    so.SetID(i);
                    Debug.Log($"Found asset {so.UnityObject.name} in the lookup table {name} at index {i} so adjusted it's ID to match", so.UnityObject);
                    OnObjectIdChanged?.Invoke(so);
                    return true;
                }
            }

            if (freeIndex < list.Count)
            {
                list[freeIndex] = so.UnityObject;
            } 
            else
            {
                list.Add(so.UnityObject);
            }
            so.SetID(freeIndex);

            Debug.Log($"Added new asset {so.UnityObject.name} to lookup table {name} at index {freeIndex}", so.UnityObject);
            EditorHelper.SetDirty(this);
            EditorHelper.SetDirty(so.UnityObject);
            EditorHelper.SaveAssets();
            OnObjectAdded?.Invoke(so);
            return true;
        }
    }

#if UNITY_EDITOR
    [UnityEditor.CustomEditor(typeof(ObjectLookupTable))]
    public class LookupTableEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            UnityEditor.EditorGUILayout.PropertyField(serializedObject.FindProperty("className"));
            var list = serializedObject.FindProperty("list");
            for(int i = 0;i < list.arraySize;i++)
            {
                UnityEditor.EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(i));
            }
            serializedObject.ApplyModifiedProperties();

            if (GUILayout.Button("Validate All"))
            {
                ValidateAll((ObjectLookupTable)target);
                serializedObject.UpdateIfRequiredOrScript();
                Repaint();
            }

            if (GUILayout.Button("Compress"))
            {
                Compress((ObjectLookupTable)target);
                serializedObject.UpdateIfRequiredOrScript();
                Repaint();
            }
            if (GUILayout.Button("Find All In Project"))
            {
                FindAll((ObjectLookupTable)target);
                serializedObject.UpdateIfRequiredOrScript();
                Repaint();
            }

        }

        public static void FindAll(ObjectLookupTable table)
        {

            UnityEditor.AssetDatabase.StartAssetEditing();
            try
            {
                ValidateAll(table);
                var type = table.GetElementType();

                var assets = EditorUtils.FindAssetsOfType(type);
                foreach (var path in assets)
                {
                    var so = UnityEditor.AssetDatabase.LoadAssetAtPath<Object>(path);
                    if(so != null)
                    {
                        if(so is GameObject go)
                        {
                            foreach(var soWithId in go.GetComponents(type))
                            {
                                table.ValidateEntry(soWithId);
                            }
                        }
                        else
                        {
                            table.ValidateEntry(so);
                        }
                    }
                }
            } 
            finally
            {
                UnityEditor.AssetDatabase.StopAssetEditing();
                UnityEditor.AssetDatabase.Refresh();

            }
        }

        public static void Compress(ObjectLookupTable table)
        {
            for (int i = table.list.Count - 1; i >= 0; i--)
            {
                if (table.list[i] == null || table.list[i] is not IObjectWithID soWithId || soWithId.UnityObject != table.list[i])
                {
                    table.list.RemoveAt(i);
                    EditorHelper.SetDirty(table);
                }
            }
            ValidateAll(table);
        }
        public static void ValidateAll(ObjectLookupTable table)
        {
            var type = table.GetElementType();
            for (int i = table.list.Count - 1; i >= 0; i--)
            {
                if (table.list[i] != null)
                {
                    if (!type.IsAssignableFrom(table.list[i].GetType()) || table.list[i] is not IObjectWithID soWithId || soWithId.UnityObject != table.list[i])
                    {
                        table.ClearAtIndex(i);
                        EditorHelper.SetDirty(table);
                        continue;
                    }  

                    if(soWithId.Id != i)
                    {
                        if (soWithId.Id >= 0 && soWithId.Id < table.list.Count && table.list[soWithId.Id] == table.list[i]) //If duplicate and duplicate is already correct id, remove this one
                        {
                            table.ClearAtIndex(i);
                        } 
                        else
                        {
                            soWithId.SetID(i);
                            table.NotifyIDChange(soWithId);
                            EditorHelper.SetDirty(table.list[i]);
                        }
                    }
                }
            }

            EditorHelper.SaveAssets();
        }
    }
#endif
}
