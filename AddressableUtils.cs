﻿#if ADDRESSSABLES
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using System.Diagnostics;



#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.AddressableAssets.Settings;
#endif
#endif

namespace SMDev.Unitilities
{
    public static partial class AddressableUtils
    {
#if ADDRESSSABLES
#if DEBUG
        static Stopwatch s = new Stopwatch();
#endif
        public static AsyncOperationHandle PreLoad<T>(this AssetReference addressable) where T : class
        {
            if (!addressable.OperationHandle.IsValid())
            {
                addressable.LoadAssetAsync<T>();
//#if DEBUG
//                UnityEngine.Debug.Log($"Preloading: {addressable}");
//#endif
            }
            return addressable.OperationHandle;
        }
        public static T LoadSynchronous<T>(this AssetReference addressable) where T : class
        {
            if(addressable.Asset != null)
            {
//#if DEBUG
//                UnityEngine.Debug.Log($"Preload of {addressable} successfull", addressable.Asset);
//#endif
                return addressable.Asset as T;
            }
#if DEBUG
            bool preloadStarted = true;
#endif
            if (!addressable.OperationHandle.IsValid())
            {
                addressable.LoadAssetAsync<T>();
#if DEBUG
                preloadStarted = false;
#endif
            }
#if DEBUG
            s.Restart();

#endif
            var obj = addressable.OperationHandle.WaitForCompletion();

#if DEBUG
            s.Stop();
            UnityEngine.Debug.LogWarning($"{addressable} loaded synchronously. Took {s.ElapsedMilliseconds}ms. Preload started: {preloadStarted}", obj as UnityEngine.Object);

#endif
            return obj as T;
        }
#if UNITY_EDITOR
        public static AddressableAssetEntry GetComponentReference<U>(U component, bool canMakeAddressable = true, AddressableAssetGroup group = null) where U : UnityEngine.Object
        {
            if (AssetDatabase.TryGetGUIDAndLocalFileIdentifier(component, out var guid, out long localId))
            {
                //Use this object to manipulate addressables
                var settings = UnityEditor.AddressableAssets.AddressableAssetSettingsDefaultObject.Settings;

                if(group == null)
                {
                    group = settings.DefaultGroup;
                }

                if (canMakeAddressable)
                {
                    var entry = settings.CreateOrMoveEntry(guid, group);

                    settings.SetDirty(AddressableAssetSettings.ModificationEvent.EntryMoved, entry, true);
                    AssetDatabase.SaveAssets();

                    return entry;
                } 
                else
                {
                    return settings.FindAssetEntry(guid);
                }
            }
            return null;
        }
#endif
#endif
    }
}
