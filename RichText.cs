﻿using UnityEngine;

namespace SMDev.Unitilities
{
    public static class RichText
    {
        public static string RT_Tag(this string text, string tag, string value = null)
        {
            if(string.IsNullOrEmpty(value))
                return $"<{tag}>{text}</{tag}>";
            else 
                return $"<{tag}={value}>{text}</{tag}>";
        }
        public static string RT_Bold(this string text)
        {
            return RT_Tag(text, "b");
        }

        public static string RT_Italic(this string text)
        {
            return RT_Tag(text, "i");
        }
        public static string RT_Color(this string text, string hexColor)
        {
            return RT_Tag(text, "color", hexColor);
        }
        public static string RT_Color(this string text, Color color)
        {
            return RT_Tag(text, "color","#"+color.ToHexString());
        }
        public static string RT_Size(this string text, int size)
        {
            return RT_Tag(text, "size", size.ToString());
        }

        public static string ToHexString(this Color color, bool includeAlpha = true)
        {
            return $"{(int)(color.r * 255):X2}{(int)(color.g * 255):X2}{(int)(color.b * 255):X2}{(int)(color.a * 255):X2}";
        }
    }
}