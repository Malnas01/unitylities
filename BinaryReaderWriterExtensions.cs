﻿using System;
using System.Collections.Generic;
using System.IO;

namespace SMDev.Unitilities
{
    public static  class BinaryReaderWriterExtensions 
    {
        public static void WriteArray(this BinaryWriter writer, ICollection<bool> array)
        {
            writer.Write(array.Count);
            foreach (var item in array)
            {
                writer.Write(item);
            }
        }
        public static void WriteArray(this BinaryWriter writer, ICollection<byte> array)
        {
            writer.Write(array.Count);
            foreach (var item in array)
            {
                writer.Write(item);
            }
        }
        public static void WriteArray(this BinaryWriter writer, ICollection<char> array)
        {
            writer.Write(array.Count);
            foreach (var item in array)
            {
                writer.Write(item);
            }
        }
        public static void WriteArray(this BinaryWriter writer, ICollection<decimal> array)
        {
            writer.Write(array.Count);
            foreach (var item in array)
            {
                writer.Write(item);
            }
        }
        public static void WriteArray(this BinaryWriter writer, ICollection<double> array)
        {
            writer.Write(array.Count);
            foreach (var item in array)
            {
                writer.Write(item);
            }
        }
        public static void WriteArray(this BinaryWriter writer, ICollection<float> array)
        {
            writer.Write(array.Count);
            foreach (var item in array)
            {
                writer.Write(item);
            }
        }
        public static void WriteArray(this BinaryWriter writer, ICollection<int> array)
        {
            writer.Write(array.Count);
            foreach (var item in array)
            {
                writer.Write(item);
            }
        }
        public static void WriteArray(this BinaryWriter writer, ICollection<long> array)
        {
            writer.Write(array.Count);
            foreach (var item in array)
            {
                writer.Write(item);
            }
        }
        public static void WriteArray(this BinaryWriter writer, ICollection<sbyte> array)
        {
            writer.Write(array.Count);
            foreach (var item in array)
            {
                writer.Write(item);
            }
        }
        public static void WriteArray(this BinaryWriter writer, ICollection<short> array)
        {
            writer.Write(array.Count);
            foreach (var item in array)
            {
                writer.Write(item);
            }
        }
        public static void WriteArray(this BinaryWriter writer, ICollection<string> array)
        {
            writer.Write(array.Count);
            foreach (var item in array)
            {
                writer.Write(item);
            }
        }
        public static void WriteArray(this BinaryWriter writer, ICollection<uint> array)
        {
            writer.Write(array.Count);
            foreach (var item in array)
            {
                writer.Write(item);
            }
        }
        public static void WriteArray(this BinaryWriter writer, ICollection<ulong> array)
        {
            writer.Write(array.Count);
            foreach (var item in array)
            {
                writer.Write(item);
            }
        }
        public static void WriteArray(this BinaryWriter writer, ICollection<ushort> array)
        {
            writer.Write(array.Count);
            foreach (var item in array)
            {
                writer.Write(item);
            }
        }

        public static T[] ReadArray<T>(this BinaryReader reader) where T : unmanaged
        {
            unsafe
            {
                int count = reader.ReadInt32();
                int byteCount = count * sizeof(T);
                var bytes = reader.ReadBytes(byteCount);
                var arr = new T[count];
                Buffer.BlockCopy(bytes, 0, arr, 0, bytes.Length);
                return arr;
            }
        }

    }
}