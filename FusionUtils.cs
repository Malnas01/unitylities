﻿#if FUSION2
using Fusion;

namespace SMDev.Unitilities
{
    public static class FusionUtils
    {
        public static bool IsSpawned(this NetworkObject networkObject)
        {
            return networkObject != null && networkObject.IsValid;
        }

        public static bool IsSpawned(this NetworkBehaviour networkBehaviour)
        {
            return networkBehaviour.Object.IsSpawned();
        }
    }
}
#endif