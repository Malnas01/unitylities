﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace SMDev.Unitilities
{

    public static partial class Extensions
    {
        public static Color Alpha(this Color color, float alpha)
        {
            color.a = alpha;
            return color;
        }

        //public static T RandomElement<T>(this IReadOnlyList<T> me)
        //{
        //    if (me.Count == 0) return default;
        //    return me[Random.Range(0, me.Count)];
        //}

        public static T RandomElement<T>(this IList<T> me)
        {
            if (me.Count == 0) return default;
            return me[Random.Range(0, me.Count)];
        }

        public static unsafe void WriteBytesUnsafe<T>(this Span<byte> me, T data, int offset) where T : unmanaged
        {
            fixed (byte * ptr = me)
            {
                *(T*)ptr[offset] = data;
            }
        }
        public static unsafe T ReadBytesUnsafe<T>(this Span<byte> me, int offset) where T : unmanaged
        {
            fixed (byte* ptr = me)
            {
                return *(T*) ptr[offset];
            }
        }

        public static float Max(this Vector3 me)
        {
            return Mathf.Max(me.x, me.y, me.z);
        }
        public static float Min(this Vector3 me)
        {
            return Mathf.Min(me.x, me.y, me.z);
        }
        public static Vector2 XY(this Vector3 me)
        {
            return new Vector2(me.x, me.y);
        }
        public static Vector2 XZ(this Vector3 me)
        {
            return new Vector2(me.x, me.z);
        }
        public static Vector2 YZ(this Vector3 me)
        {
            return new Vector2(me.y, me.z);
        }

        public static Vector2 YX(this Vector3 me)
        {
            return new Vector2(me.y, me.x);
        }
        public static Vector2 ZX(this Vector3 me)
        {
            return new Vector2(me.z, me.x);
        }
        public static Vector2 ZY(this Vector3 me)
        {
            return new Vector2(me.z, me.y);
        }

        public static float Max(this Vector2 me)
        {
            return Mathf.Max(me.x, me.y);
        }

        public static float Min(this Vector2 me)
        {
            return Mathf.Min(me.x, me.y);
        }

        public static Vector3 AsXZ(this Vector2 me)
        {
            return new Vector3(me.x, 0, me.y);
        }
        public static Vector3 AsYZ(this Vector2 me)
        {
            return new Vector3(0, me.x, me.y);
        }
        public static Vector3 AsZX(this Vector2 me)
        {
            return new Vector3(me.y, 0, me.x);
        }
        public static Vector3 AsZY(this Vector2 me)
        {
            return new Vector3(0, me.y, me.x);
        }
        public static Vector3 AsYX(this Vector2 me)
        {
            return new Vector3(me.y, me.x, 0);
        }
        public static Vector3 AsXY(this Vector2 me)
        {
            return new Vector3(me.x, me.y, 0);
        }
        /// <summary>
        /// Iterates all children of a game object
        /// </summary>
        /// <param name="gameObject">A root game object</param>
        /// <param name="childHandler">A function to execute on each child</param>
        /// <param name="recursive">Do it on children? (in depth)</param>
        public static void IterateChildren(this Transform transform, Action<Transform> childHandler, bool recursive)
        {
            foreach (Transform child in transform)
            {
                childHandler(child);
                if (recursive) child.IterateChildren(childHandler, true);
            }
        }

        public static IEnumerable<Transform> AllChildrenRecursive(this Transform transform)
        {
            foreach (Transform child in transform)
            {
                yield return child;
                foreach(Transform c in child.AllChildrenRecursive())
                {
                    yield return c;
                }
            }
        }

        public static void Shuffle<T>(this IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = Random.Range(0, n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static bool ContainsLayer(this LayerMask layerMask, int layer)
        {
            return layerMask.value == (layerMask.value | (1 << layer));
        }

        public static void ValidateComponentSelf<T>(this MonoBehaviour me, ref T component)
        {
            if(Equals(component, null))
            {
                component = me.GetComponent<T>();
                if (Equals(component, null))
                {
                    Debug.LogWarning($"No component {typeof(T)} found for script {me.GetType()} on {me.name}",me);
                }
            }
        }
        public static void ValidateComponentInChildren<T>(this MonoBehaviour me, ref T component)
        {
            if (Equals(component, null))
            {
                component = me.GetComponentInChildren<T>();
                if (Equals(component, null))
                {
                    Debug.LogWarning($"No component {typeof(T)} found for script {me.GetType()} on {me.name}", me);
                }
            }
        }

        public static void ValidateComponentInParent<T>(this MonoBehaviour me, ref T component)
        {
            if (Equals(component, null))
            {
                component = me.GetComponentInParent<T>();
                if (Equals(component, null))
                {
                    Debug.LogWarning($"No component {typeof(T)} found for script {me.GetType()} on {me.name}", me);
                }
            }
        }

        public static Vector3 ToVector3(this Vector3Int vector3Int)
        {
            return new Vector3(vector3Int.x, vector3Int.y, vector3Int.z);
        }

        public static Vector3Int ToVector3Int(this Vector3 vector3, bool floor = true)
        {
            if(floor)
            {
                return new Vector3Int(Mathf.FloorToInt(vector3.x), Mathf.FloorToInt(vector3.y), Mathf.FloorToInt(vector3.z));
            } 
            else
            {
                return new Vector3Int((int)vector3.x, (int)vector3.y, (int)vector3.z);
            }
        }

        public static T ChooseRandomWeighted<T>(this List<T> items, Func<T, float> weight) => ChooseRandomWeighted<T>(items, weight, UnityEngine.Random.value);
        public static T ChooseRandomWeighted<T>(this List<T> items, Func<T, float> weight, float randomValueBetween0And1)
        {
            var index = Utils.ChooseIndexWeighted(items.Count, i => weight(items[i]), randomValueBetween0And1);
            return items[index];
            //var sum = items.Sum(x => weight(x));
            //var ran = randomValueBetween0And1 * sum;

            //foreach (var i in items)
            //{
            //    ran -= weight(i);
            //    if (ran <= 0)
            //    {
            //        return i;
            //    }
            //}
            //return items.Last();
        }
        public static T ChooseRandom<T>(this List<T> array)
        {
            return array.Count > 0 ? array[Random.Range(0, array.Count)] : default(T);
        }
        public static T ChooseRandom<T>(this List<T> array, float randomValueBetween0And1)
        {
            int element = Mathf.Min(Mathf.FloorToInt(randomValueBetween0And1 * array.Count), array.Count - 1);
            return array.Count > 0 ? array[element] : default(T);
        }
        public static T ChooseRandom<T>(this T[] array, float randomValueBetween0And1)
        {
            int element = Mathf.Min(Mathf.FloorToInt(randomValueBetween0And1 * array.Length), array.Length - 1);
            return array.Length > 0 ? array[element] : default(T);
        }
        public static T ChooseRandom<T>(this T[] array)
        {
            return array.Length > 0 ? array[Random.Range(0, array.Length)] : default(T);
        }
    }
}
