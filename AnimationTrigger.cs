﻿using System.Collections.Generic;
using UnityEngine;

namespace SMDev.Unitilities
{

    [System.Serializable]
    public abstract class AnimationTrigger
    {
        public abstract void Trigger(Animator animator);
    }
    public static class AnimationTriggerExtensions
    {
        public static void TryTrigger(this AnimationTrigger player, Animator animator)
        {
            if (animator != null && player != null)
            {
                player.Trigger(animator);
            }
        }
    }

    public class AnimatonTriggerBoolParameter : AnimationTrigger
    {
        public string boolParameterName;
        public bool triggerValue = true;

        public override void Trigger(Animator animator)
        {
            animator.SetBool(boolParameterName, triggerValue);
        }
    }

    public class AnimationTriggerPlayState : AnimationTrigger
    {
        public int layer = -1;
        public string animationState;

        public override void Trigger(Animator animator)
        {
            animator.Play(animationState);
        }
    }

    public class AnimationTriggerTriggerParameter : AnimationTrigger
    {
        public string triggerParamaterName;

        public override void Trigger(Animator animator)
        {
            animator.SetTrigger(triggerParamaterName);
        }
    }
    public class AnimationTriggerSO : AnimationTrigger
    {
        [EditInline]
        public AnimationTriggerScriptableObject ScriptableObject;

        public override void Trigger(Animator animator)
        {
            if (ScriptableObject != null)
            {
                ScriptableObject.Settings.Trigger(animator);
            }
        }
    }

    public class MultiTrigger : AnimationTrigger
    {
        [SerializeReference, ChooseableSubtype]
        public List<AnimationTrigger> triggers;

        public override void Trigger(Animator animator)
        {
            foreach (var t in triggers)
            {
                t.Trigger(animator);
            }
        }
    }
}