﻿using UnityEngine;

namespace SMDev.Unitilities
{
    [CreateAssetMenu(menuName = "Animation Definition/Animation Trigger Scriptable Object")]
    public class AnimationTriggerScriptableObject : ScriptableObject
    {
        [SerializeReference, ChooseableSubtype]
        public AnimationTrigger Settings;
    }
}