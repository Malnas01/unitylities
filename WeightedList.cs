﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using System.Reflection;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditorInternal;
#endif

namespace SMDev.Unitilities
{
    public class WeightedListElementNameAttribute : Attribute
    {
        public string NameOrPropertyPath;
        public bool NameIsPropertyPath;
        public WeightedListElementNameAttribute(string name, bool nameIsPropertyPath = true)
        {
            NameOrPropertyPath = name;
            NameIsPropertyPath = nameIsPropertyPath;
        }
    }

    public class WeightNameAttribute : PropertyAttribute
    {
        public readonly string WeightName;
        public WeightNameAttribute(string weightName)
        {
            WeightName = weightName;
        }
    }

    public interface IWeightedList
    {
        void SortByWeight();
        float TotalWeight { get; }

        int GetIndexForWeight(float weight);
    }

    [System.Serializable]
    public class HardcodedWeightedList<T> : WeightedList<T>
    {
        [HideInInspector]
        [SerializeField]
        T[] elements;
        public HardcodedWeightedList(params T[] elements) 
        {
            this.elements = elements;

            Elements = new List<WeightedListElement<T>>(elements.Select(x=>new WeightedListElement<T>() { weight = 1, element = x })); 
        }

        protected override void OnBeforeSerialize()
        {
            if (elements.Length != Elements.Count)
            {
                Elements = new List<WeightedListElement<T>>(elements.Select(x => new WeightedListElement<T>() { weight = 1, element = x }));
            }
            else
            {
                for (int i = 0; i < elements.Length; i++)
                {
                    Elements[i].element = elements[i];
                }
            }

            base.OnBeforeSerialize();
        }
    }
    [System.Serializable]
    public class WeightedListElement<T>
    {
        public float weight = 1;

        public T element;
        public WeightedListElement()
        {
            this.weight = 1;
            this.element = default(T);
        }
        public WeightedListElement(T element)
        {
            this.weight = 1;
            this.element = element;
        }
        public WeightedListElement(float weight, T element)
        {
            this.weight = weight;
            this.element = element;
        }

        public void Deconstruct(out float weight, out T element)
        {
            weight = this.weight;
            element = this.element;
        }
        public static implicit operator WeightedListElement<T>((float weight, T element) tuple) => new WeightedListElement<T>(tuple.weight, tuple.element);
    }
    [System.Serializable]
    public partial class WeightedList<T> : ISerializationCallbackReceiver, IWeightedList
    {
        public WeightedList()
        {
            Elements = new List<WeightedListElement<T>>();
        }

        public static WeightedList<T> FromElements(params T[] elements)
        {
            var list = new WeightedList<T>()
            {
                Elements = new List<WeightedListElement<T>>(elements.Select(x => new WeightedListElement<T>(x)))
            };
            list.CalculateTotalWeight();
            return list;
        }

        public static WeightedList<T> FromWeightedElements(params (float weight, T element)[] weightedElements)
        {
            var list = new WeightedList<T>()
            {
                Elements = new List<WeightedListElement<T>>(weightedElements.Select(x => (WeightedListElement<T>)x))
            };
            list.CalculateTotalWeight();
            return list;
        }

        [SerializeField]
        float totalWeight;

        public float TotalWeight => totalWeight;

        public List<WeightedListElement<T>> Elements = new List<WeightedListElement<T>>();
        public void SortByWeight() => Elements.Sort((x,y) => x.weight.CompareTo(y.weight));
        
        public int Count => Elements.Count;
        public T this[int i]
        {
            get => Elements[i].element;
            set => Elements[i].element = value;
        }

        public int GetIndexForWeight(float weight)
        {
            for (int i = 0; i < Elements.Count; i++)
            {
                WeightedListElement<T> entry = Elements[i];
                weight -= entry.weight;
                if (weight <= 0)
                {
                    return i;
                }
            }
            return Elements.Count - 1;
        }

        public IEnumerator<T> RandomOrder()
        {
            var list = new WeightedList<T>();
            list.Elements.AddRange(Elements);
            list.CalculateTotalWeight();
            
            while (list.Count > 0)
            {
                var index = list.GetRandomElementIndex();
                var element = list.Elements[index];
                list.Elements.RemoveAt(index);
                list.totalWeight -= element.weight;
                yield return element.element;
            }
        }

        public T GetElementAtWeight(float weight)
        {
            return Elements[GetIndexForWeight(weight)].element;
        }
        public T GetHighestWeightedElement(float weight)
        {
            return Elements[GetHighestWeightedIndex(weight)].element;
        }
        public int GetHighestWeightedIndex(float weightLessThanOrEqual)
        {
            int highest = 0;
            for(int i = 0; i < Elements.Count; i++)
            {
                if (Elements[i].weight > Elements[highest].weight && Elements[i].weight <= weightLessThanOrEqual)
                {
                    highest = i;
                }
            }

            return highest;
        }

        protected virtual void OnBeforeSerialize() {

        }
        protected virtual void OnAfterDeserialize()
        {
            CalculateTotalWeight();
        }

        public void CalculateTotalWeight()
        {
            totalWeight = Elements.Sum(x => x.weight);
        }

        void ISerializationCallbackReceiver.OnBeforeSerialize() => OnBeforeSerialize();
        void ISerializationCallbackReceiver.OnAfterDeserialize() => OnAfterDeserialize();
        public int GetRandomElementIndex(float randomValueBetween0And1)
        {
            if(totalWeight == 0)
            {
                if(randomValueBetween0And1 == 1)
                {
                    return Elements.Count - 1;
                }

                return (int)(randomValueBetween0And1 * Elements.Count);
            }
            return GetIndexForWeight(randomValueBetween0And1 * totalWeight);
        }
        public T GetRandomElement(float randomValueBetween0And1)
        {
            if (Elements.Count == 0)
            {
                return default(T);
            }
            var index = GetRandomElementIndex(randomValueBetween0And1);
            return Elements[index].element;
        }
        public int GetRandomElementIndex() => GetRandomElementIndex(UnityEngine.Random.value);
        public T GetRandomElement() => GetRandomElement(UnityEngine.Random.value);
    }

#if UNITY_EDITOR

    public abstract class CustomReorderableListDrawer : PropertyDrawer
    {
        public abstract ReorderableList CreateList(SerializedProperty property);
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            if (!ReorderableListCache.TryGetReorderableList(property, out var list))
            {
                list = CreateList(property);
                ReorderableListCache.CacheList(property, list);
            }
            var height = list.GetHeight();
            return height;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (!ReorderableListCache.TryGetReorderableList(property, out var list))
            {
                list = CreateList(property);
                ReorderableListCache.CacheList(property, list);
            }
            list.DoList(position);
            property.serializedObject.ApplyModifiedProperties();
        }
    }
    public static class ReorderableListCache
    {
        static Dictionary<string, WeakReference<ReorderableList>> listCache = new Dictionary<string, WeakReference<ReorderableList>>();
        public static string GetPropertyIdentifier(SerializedProperty serializedProperty)
        {
            try
            {
                return serializedProperty?.propertyPath + serializedProperty.serializedObject.targetObject.GetInstanceID();
            }
            catch (NullReferenceException)
            {
                return string.Empty;
            }
        }

        public static bool TryGetReorderableList(SerializedProperty property, out ReorderableList list)
        {
            var key = GetPropertyIdentifier(property);
            if (listCache.TryGetValue(key, out var listReference))
            {
                if (listReference.TryGetTarget(out list))
                {
                    if (list.serializedProperty.serializedObject.HasBeenDisposed())
                    {
                        list = null;
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            list = null;
            return false;
        }

        public static void CacheList(SerializedProperty property, ReorderableList list)
        {
            var key = GetPropertyIdentifier(property);
            if (listCache.TryGetValue(key, out var listReference))
            {
                listReference.SetTarget(list);
            }
            else
            {
                listCache[key] = new WeakReference<ReorderableList>(list);
            }
        }
    }

    [CustomPropertyDrawer(typeof(HardcodedWeightedList<>), true)]
    public class HardcodedWeightedListDrawer : WeightedListDrawer
    {
        protected override ReorderableList GetList(SerializedProperty property) => WeightedListReorderableList.GetReorderableList(property, false, true, false, false, false);

    }

    [CustomPropertyDrawer(typeof(WeightedList<>), true)]
    public class WeightedListDrawer : PropertyDrawer
    {
        protected virtual ReorderableList GetList(SerializedProperty property) => WeightedListReorderableList.GetReorderableList(property);

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            var list = GetList(property);
            float height = 0;
            if (property.isExpanded == false)
            {
                height = list.headerHeight;
            }
            else
            {
                height = list.GetHeight();
            }
            return height;
        }

        private void DoListHeader(Rect headerRect, SerializedProperty element)
        {
            var list = GetList(element);
            headerRect.height = list.headerHeight;
            if (element != null)
            {
                EditorGUI.BeginProperty(headerRect, GUIContent.none, element);
            }

            if (Event.current.type == EventType.Repaint)
            {
                ReorderableList.defaultBehaviours.DrawHeaderBackground(headerRect);
            }

            headerRect.xMin += 6f;
            headerRect.xMax -= 6f;
            headerRect.height -= 2f;
            headerRect.y += 1f;

            list.drawHeaderCallback.Invoke(headerRect);

            if (element != null)
            {
                EditorGUI.EndProperty();
            }
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUIUtility.hierarchyMode = true;
            var foldoutRect = position;
            foldoutRect.width = EditorStyles.foldout.padding.left;
            property.isExpanded = EditorGUI.Foldout(foldoutRect, property.isExpanded, label);
            if (property.isExpanded == false)
            {
                DoListHeader(position, property);
            } 
            else
            {
                GetList(property).DoList(position);
            }
            property.serializedObject.ApplyModifiedProperties();
        }

        public static string GetPropertyIdentifier(SerializedProperty serializedProperty)
        {
            try
            {
                return serializedProperty?.propertyPath + serializedProperty.serializedObject.targetObject.GetInstanceID();
            }
            catch (NullReferenceException)
            {
                return string.Empty;
            }
        }

        public class WeightedListReorderableList
        {
            static Dictionary<string, WeakReference<ReorderableList>> listCache = new Dictionary<string, WeakReference<ReorderableList>>();

            private SerializedProperty property;

            ReorderableList list;
            public ReorderableList List => list;

            bool drawOrderByButton = true;

            public static ReorderableList GetReorderableList(SerializedProperty property, bool draggable = true, bool displayHeader = true, bool displayAddButton = true, bool displayRemoveButton = true, bool drawOrderByButton = true)
            {
                ReorderableList list = null;
                var key = GetPropertyIdentifier(property);
                if (listCache.TryGetValue(key, out var listReference))
                {
                    if (listReference.TryGetTarget(out list))
                    {
                        if (list.serializedProperty.serializedObject.HasBeenDisposed())
                        {
                            list = null;
                        }
                    }
                }

                if (list == null)
                {
                    var weightedListReorderableList = new WeightedListReorderableList(property, draggable, displayHeader, displayAddButton, displayRemoveButton, drawOrderByButton);
                    list = weightedListReorderableList.List;
                    if (listReference != null)
                    {
                        listReference.SetTarget(list);
                    }
                    else
                    {
                        listCache[key] = new WeakReference<ReorderableList>(list);
                    }
                }
                return list;
            }
            public WeightedListReorderableList(SerializedProperty property, bool draggable = true, bool displayHeader = true, bool displayAddButton = true, bool displayRemoveButton = true, bool drawOrderByButton = true)
            {
                this.property = property;
                list = new ReorderableList(property.serializedObject, property.FindPropertyRelative("Elements"), draggable, displayHeader, displayAddButton, displayRemoveButton);
                list.drawHeaderCallback = DrawHeader;
                list.elementHeightCallback = ElementHeightCallback;
                list.drawElementCallback = ElementDrawCallback;
                this.drawOrderByButton = drawOrderByButton;
            }

            private float ElementHeightCallback(int index)
            {
                if (property == null)
                {
                    throw new NullReferenceException($"Tried to get element height but {nameof(property)} was null");
                }
                var elements = property.FindPropertyRelative("Elements");
                var prop = elements.GetArrayElementAtIndex(index);
                var element = prop.FindPropertyRelative("element");
                if (element == null)
                {
                    return EditorGUIUtility.singleLineHeight;
                }
                var elementHeight = EditorGUI.GetPropertyHeight(element, true);
                return elementHeight + ((EditorGUIUtility.wideMode && !ShouldDrawInNarrowMode(element)) ? 0f : (EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing));
            }

            string GetWeightString()
            {
                var customNames = property.GetAttributes<WeightNameAttribute>(false);

                if (customNames.Length > 0)
                {
                    return customNames[0].WeightName;
                }
                return "Weight";
            }

            private void DrawHeader(Rect rect)
            {
                if (property == null)
                {
                    throw new NullReferenceException($"Tried to draw header but {nameof(property)} was null");
                }

                using var scope = new EditorGUI.IndentLevelScope(-EditorGUI.indentLevel);
                //EditorGUI.indentLevel = 0;

                var labelRect = new Rect(rect);
                var propertyName = new GUIContent(property.displayName);
                labelRect.width = EditorStyles.label.CalcSize(propertyName).x + 20;
                EditorGUI.LabelField(labelRect, propertyName);

                var orderByWeightRect = new Rect(rect);
                var weightName = GetWeightString();

                var orderByWeightLabel = new GUIContent($"Order by {weightName}");
                var labelWidth = EditorStyles.label.CalcSize(orderByWeightLabel).x;
                orderByWeightRect.width = labelWidth + 10f;
                orderByWeightRect.x += labelRect.width;
                orderByWeightRect.height -= 2;
                orderByWeightRect.y += 1;

                if (drawOrderByButton)
                {
                    if (GUI.Button(orderByWeightRect, orderByWeightLabel))
                    {
                        property.TraverseSerializedPropertyPath(out _, out var objectValue, out _);
                        if (objectValue is IWeightedList sortByWeight)
                        {
                            Undo.RecordObject(property.serializedObject.targetObject,$"{orderByWeightLabel.ToString()} {property.propertyPath} in {property.serializedObject.targetObject.name}");
                            sortByWeight.SortByWeight();
                            PrefabUtility.RecordPrefabInstancePropertyModifications(property.serializedObject.targetObject);
                        }
                    }
                }


                var totalWeightRect = new Rect(rect);
                var totalWeight = property.FindPropertyRelative("totalWeight");
                var totalWeightContent = new GUIContent($"Total {weightName}: {totalWeight.floatValue}");

                totalWeightRect.xMin = totalWeightRect.xMax - EditorStyles.label.CalcSize(totalWeightContent).x;

                var style = new GUIStyle(EditorStyles.label);
                style.alignment = TextAnchor.MiddleRight;
                EditorGUI.LabelField(totalWeightRect, totalWeightContent, style);

                if (Event.current.type == EventType.MouseUp && rect.Contains(Event.current.mousePosition) && !orderByWeightRect.Contains(Event.current.mousePosition))
                {
                    property.isExpanded = !property.isExpanded;
                    InvalidateRelevantLists();
                    Event.current.Use();
                }
            }

            void InvalidateRelevantLists()
            {
                var method = typeof(ReorderableList).GetMethod("InvalidateCacheRecursive", EditorUtils.AllBindingFlags);
                
                foreach (var s_reorderableList in listCache)
                {
                    if (s_reorderableList.Key.Contains(property.propertyPath.Split('.')[0]) && s_reorderableList.Key.Contains(property.serializedObject.targetObject.GetInstanceID().ToString()))
                    {
                        if(s_reorderableList.Value.TryGetTarget(out var list))
                        {
                            method.Invoke(list, null);
                        }
                    }
                }
            }

            bool ShouldDrawInNarrowMode(SerializedProperty element)
            {
                return element.type.Contains("WeightedList") || PropertyHandler.GetHandler(element).propertyDrawer != null;
                    //return type.IsGenericType && type.GetGenericTypeDefinition() == typeof(WeightedList<>);
            }
            private void ElementDrawCallback(Rect rect, int index, bool isActive, bool isFocused)
            {
                if (property == null)
                {
                    throw new NullReferenceException($"Tried to draw list elements but {nameof(property)} was null");
                }
                var elements = property.FindPropertyRelative("Elements");
                if (index >= elements.arraySize)
                {
                    return;
                }

                using var scope = new EditorGUI.IndentLevelScope(-EditorGUI.indentLevel);
                //EditorGUI.indentLevel = 0;

                var prop = elements.GetArrayElementAtIndex(index);
                var totalWeight = property.FindPropertyRelative("totalWeight");
                var weight = prop.FindPropertyRelative("weight");
                var element = prop.FindPropertyRelative("element");

                if (element == null)
                {
                    prop.TraverseSerializedPropertyPath(out _, out _, out var enclosingType);

                    EditorGUI.LabelField(rect, new GUIContent($"{nameof(element)} is null. Make sure the type {enclosingType.GenericTypeArguments.FirstOrDefault()?.FullName} is System.Serializable"));
                    return;
                }

                var weightName = GetWeightString();

                float chance = 0f;
                if(totalWeight.floatValue == 0)
                {
                    chance = 1f / elements.arraySize;
                }
                else
                {
                    chance = weight.floatValue / totalWeight.floatValue;
                }
                var weightLabel = new GUIContent($"{weightName} ({chance:P})");

                float weightLabelWidth = EditorStyles.label.CalcSize(weightLabel).x;

                string label = null;

                element.TraverseSerializedPropertyPath(out _, out var elementObject, out var elementType);

                //First try get name attribute from where the WeightedList property is declared
                var customElementNameAttributes = property.GetAttributes<WeightedListElementNameAttribute>(true);

                //If there's no attribute on the WeightedList property, try get it from the element type
                if (customElementNameAttributes.Length == 0)
                {
                    customElementNameAttributes = elementType.GetCustomAttributes<WeightedListElementNameAttribute>(true).ToArray();
                }

                //If we found a ListNameElementAttribute try get the element name
                if (customElementNameAttributes.Length > 0)
                {
                    if (customElementNameAttributes[0].NameIsPropertyPath)
                    {
                        var nameObject = EditorUtils.GetFieldPropertyOrReturnValue(elementObject, customElementNameAttributes[0].NameOrPropertyPath);
                        label = nameObject?.ToString();
                    }
                    else
                    {
                        label = string.Format(customElementNameAttributes[0].NameOrPropertyPath, index);
                    }
                }
                else
                {
                    //Try get name using CustomListElementNameAttribute
                    var customListElementNameAttributes = property.GetAttributes<CustomListElementName>(true);

                    if (customListElementNameAttributes.Length > 0)
                    {
                        label = customListElementNameAttributes[0].GetName(property, index);
                    }
                }
                //If no name was found default to Element {index}
                if (string.IsNullOrEmpty(label))
                {
                    label = $"{element.displayName} {index}";
                }

                var labelContent = new GUIContent(label);

                float elementLabelWidth = EditorStyles.label.CalcSize(labelContent).x;

                EditorGUIUtility.labelWidth = weightLabelWidth;
                var weightRect = new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight);

                const float weightElementBuffer = 20;

                if (EditorGUIUtility.wideMode && !ShouldDrawInNarrowMode(element))
                {
                    weightRect.width /= 2.4f;
                    EditorGUI.PropertyField(weightRect, weight, weightLabel);

                    var propertyRect = new Rect(rect.x + weightRect.width + weightElementBuffer, rect.y, rect.width - weightRect.width - weightElementBuffer, EditorGUIUtility.singleLineHeight);

                    EditorGUIUtility.labelWidth = elementLabelWidth;
                    if (property.type.Contains("HardcodedWeightedList"))
                    {
                        GUI.enabled = false;    
                    }
                                 
                    //We draw the element without children first
                    if (EditorGUI.PropertyField(propertyRect, element, labelContent, false))
                    {

                        //Then if the element has children and is expanded we manually draw the children so we can draw them underneath the whole space instead of underneath just the element side
                        EditorGUIUtility.labelWidth = 0;
                        const float elementChildrenIndent = 15f;

                        var elementChildRect = new Rect(rect.x + elementChildrenIndent, rect.y + EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing, rect.width - elementChildrenIndent, 0);

                        var depth = element.depth;
                        if (element.NextVisible(true) && element.depth > depth)
                        {
                            do
                            {
                                elementChildRect.height = EditorGUI.GetPropertyHeight(element);
                                EditorGUI.PropertyField(elementChildRect, element, true);
                                elementChildRect.y += elementChildRect.height + EditorGUIUtility.standardVerticalSpacing;
                            }
                            while (element.NextVisible(false) && element.depth > depth);
                        }
                    }
                }
                else
                {
                    EditorGUI.PropertyField(weightRect, weight, weightLabel);

                    var propertyRect = new Rect(rect.x, rect.y + weightRect.height + EditorGUIUtility.standardVerticalSpacing, rect.width, rect.height - weightRect.height);

                    EditorGUIUtility.labelWidth = 0;
                    if (property.type.Contains("HardcodedWeightedList"))
                    {
                        GUI.enabled = false;
                    }
                    EditorGUI.PropertyField(propertyRect, element, labelContent, true);
                }
                GUI.enabled = true;
            }
        }
    }

#endif
}