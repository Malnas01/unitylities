﻿using UnityEngine;
using Object = UnityEngine.Object;

namespace SMDev.Unitilities
{
    [System.Obsolete("Use IObjectWithID")]
    public interface IScriptableObjectWithID : IObjectWithID
    {

    }
    public partial interface IObjectWithID
    {
        public Object UnityObject { get; }
        public int Id { get; }

        void SetID(int id);

        public ObjectLookupTable GetLookup() => ObjectLookupTable.GetLookup(GetType());
    }

    public abstract partial class ScriptableObjectWithID<SelfReferenceType> : ScriptableObject, IObjectWithID where SelfReferenceType : ScriptableObject, IObjectWithID
    {
        public Object UnityObject => this;

        [field: SerializeField, ReadOnlyInspector, Button(nameof(PingLookup))]
        public int Id { get; private set; }

        public void SetID(int id)
        {
            var lastId = Id;
            Id = id;
            if(Id != lastId)
            {
                OnIdChanged(lastId);
            }
        }

        protected virtual void OnIdChanged(int lastId)
        {

        }

        public ObjectLookupTable GetLookup()
        {
            return ObjectLookupTable.GetLookup(GetType());
        }

        public void PingLookup()
        {
#if UNITY_EDITOR
            UnityEditor.EditorGUIUtility.PingObject(GetLookup());
#endif
        }


    }
}
