﻿using System;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace SMDev.Unitilities
{
#if UNITY_EDITOR && NET_4_6
    public partial class ObjectLayerGenerator
    {
        public string[] source;

        public ObjectLayerGenerator() 
        {
            source = new string[32];
            for(int i = 0; i < source.Length; i++)
            {
                source[i] = LayerMask.LayerToName(i).Replace("-","");
            }
            //source = UnityEditorInternal.InternalEditorUtility.layers;
        }

        [MenuItem("Code Generation/Generate Layers")]
        public static void GenerateLayers()
        {
            Generate("Scripts/Generated/ObjectLayers");
        }

        private static void Generate(string name)
        {
            // Build the generator with the class name and data source.
            ObjectLayerGenerator generator = new ObjectLayerGenerator();

            // Generate output (class definition).
            var classDefintion = generator.TransformText();

            var outputPath = Path.Combine(Application.dataPath, name + ".cs");

            try
            {
                EditorUtils.EnsureAssetPathExists(outputPath);
                // Save new class to assets folder.
                File.WriteAllText(outputPath, classDefintion);

                // Refresh assets.
                AssetDatabase.Refresh();
            }
            catch (Exception e)
            {
                Debug.Log("An error occurred while saving file: " + e);
            }
        }

    }
#endif
}
