﻿#if LOCALIZATION
using UnityEditor;
using UnityEngine;

using UnityEngine.Localization.Settings;
using UnityEngine.Localization.Tables;

namespace SMDev.Unitilities
{
    [CustomPropertyDrawer(typeof(InitializeLocalizedString))]
    public class LocalizedStringInitializerDrawer : PropertyDrawer 
    {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUI.GetPropertyHeight(property, label);
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.PropertyField(position, property, label);

            var initAttribute = attribute as InitializeLocalizedString;
            if(property.type == "LocalizedString")
            {
                var tableRef = property.FindPropertyRelative("m_TableReference.m_TableCollectionName");
                var tableEntryKey = property.FindPropertyRelative("m_TableEntryReference.m_KeyId");
                var tableEntryName = property.FindPropertyRelative("m_TableEntryReference.m_Key");


                if (string.IsNullOrEmpty(tableRef.stringValue) || (tableEntryKey.longValue == SharedTableData.EmptyId && string.IsNullOrEmpty(tableEntryName.stringValue)))
                {
                    if(initAttribute.Key.Contains("{name}") && string.IsNullOrEmpty(property.serializedObject.targetObject.name))
                    {
                        return;
                    }
                    var key = initAttribute.Key.Replace("{name}", property.serializedObject.targetObject.name.Replace(" ", ""));

                    foreach(var e in initAttribute.Entries)
                    {
                        var locale = LocalizationSettings.AvailableLocales.GetLocale(e.locale);
                        if(locale == null)
                        {
                            continue;
                            //locale = Locale.CreateLocale(e.locale);
                            //LocalizationSettings.AvailableLocales.AddLocale(locale);
                        }
                        var table = LocalizationSettings.StringDatabase.GetTable(initAttribute.TableName, locale);

                        if (table != null)
                        {
                            var entry = table.GetEntry(key);
                            if (entry == null)
                            {
                                entry = table.AddEntryFromReference(key, e.text.Replace("{name}", ObjectNames.NicifyVariableName(property.serializedObject.targetObject.name)));
                                UnityEditor.EditorUtility.SetDirty(table);
                                UnityEditor.EditorUtility.SetDirty(table.SharedData);
                                AssetDatabase.SaveAssetIfDirty(table);
                                AssetDatabase.SaveAssetIfDirty(table.SharedData);
                            }
                        }
                    }

                    tableRef.stringValue = initAttribute.TableName;
                    tableEntryKey.longValue = SharedTableData.EmptyId;
                    tableEntryName.stringValue = key;
                }
            }
        }
    }
}
#endif