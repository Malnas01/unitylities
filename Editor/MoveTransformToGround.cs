﻿using UnityEditor;
using UnityEditor.ShortcutManagement;
using UnityEngine;

namespace SMDev.Unitilities
{

    public static class MoveTransformToGround
    {
        [MenuItem("CONTEXT/Transform/Move To Ground #G")]
        static void ContextMenu(MenuCommand command)
        {
            MoveSelectionToGround();   
        }

        [Shortcut("Move Transform To Ground", KeyCode.G, ShortcutModifiers.Shift)]
        public static void MoveSelectionToGround()
        {
            foreach (var s in Selection.gameObjects)
            {
                if (s != null)
                {
                    MoveToGround(s.transform);
                }
            }
        }

        public static void MoveToGround(Transform transform)
        {
            if (transform != null)
            {
                var hits = Physics.RaycastAll(transform.position + Vector3.up, Vector3.down);

                float dist = float.PositiveInfinity;

                foreach (var hit in hits)
                {
                    if (hit.collider.transform.IsChildOf(transform)) continue;

                    if (hit.distance < dist)
                    {
                        dist = hit.distance;
                    }
                }

                if (dist != float.PositiveInfinity)
                {
                    Undo.RecordObject(transform, "Move To Ground");
                    transform.position = transform.position + Vector3.up + Vector3.down * dist;
                }
            }
        }
        
    }
}