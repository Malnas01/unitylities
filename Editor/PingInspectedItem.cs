﻿using UnityEditor;

namespace SMDev.Unitilities
{
    public static class ContextMenuPing
    {
        [MenuItem("CONTEXT/Object/Ping")]
        public static void Ping(MenuCommand command)
        {
            EditorGUIUtility.PingObject(command.context);
        }
    }
}