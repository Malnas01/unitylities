﻿using System.Collections.Generic;
using UnityEditor;
using UnityEditor.ShortcutManagement;
using UnityEngine;

namespace SMDev.Unitilities
{
    public static class MoveTransformToCursor
    {
        const HideFlags hideFlags = HideFlags.DontSaveInEditor;
        static float rotation;

        static Vector3 initialPos;
        static Quaternion initialRot;

        static GameObject[] SelectedGameObjects;

        static GameObject visuals;

        static List<Material> instantiatedMaterials = new List<Material>();

        static List<Material> rendererMaterialBuffer = new List<Material>();
        public static bool IsPerformingMove => SelectedGameObjects != null;

        static int undoGroup;


        [MenuItem("CONTEXT/Transform/Move To Cursor #C")]
        static void ContextMenu(MenuCommand command)
        {
            StartMove();
        }
        [Shortcut("Move Transform To Cursor", KeyCode.C, ShortcutModifiers.Shift)]
        public static void StartMove()
        {
            if (Selection.gameObjects.Length == 0)
            {
                return;
            }
            if (IsPerformingMove)
            {
                EndMove();
            }
            rotation = 0;

            SelectedGameObjects = Selection.gameObjects;
            SceneView.duringSceneGui += OnSceneGUI;

            initialPos = SelectedGameObjects[0].transform.position;
            initialRot = SelectedGameObjects[0].transform.rotation;
            visuals = new GameObject("MoveToCursor_VisualParent");
            visuals.hideFlags = hideFlags;
            visuals.layer = 2; //Default Ignore Raycast
            visuals.transform.position = initialPos;
            visuals.transform.rotation = initialRot;

            void SetMaterialsTransparent(Renderer renderer, float alpha)
            {
                rendererMaterialBuffer.Clear();
                foreach (var m in renderer.sharedMaterials)
                {
                    var mat = new Material(m);
                    mat.hideFlags = hideFlags;
                    mat.color = mat.color.Alpha(alpha);

                    mat.SetOverrideTag("RenderType", "Transparent");
                    mat.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
                    mat.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                    mat.SetInt("_ZWrite", 0);
                    mat.DisableKeyword("_ALPHAPREMULTIPLY_ON");
                    mat.EnableKeyword("_SURFACE_TYPE_TRANSPARENT");
                    mat.renderQueue = (int)UnityEngine.Rendering.RenderQueue.Transparent;
                    mat.SetShaderPassEnabled("ShadowCaster", false);
                    mat.DisableKeyword("_EMISSION");
                    mat.SetFloat("_Blend", 0.0f);
                    //// Set surface type to Transparent
                    mat.SetFloat("_Surface", 1.0f);

                    rendererMaterialBuffer.Add(mat);
                    instantiatedMaterials.Add(mat);

#if UNITY_2022_2_OR_NEWER
                    renderer.SetMaterials(rendererMaterialBuffer);
#else
                    renderer.materials = rendererMaterialBuffer.ToArray();
#endif

                }
            }

            GameObject CreateVisualCopy(GameObject g, Transform parent, string namePostfix = "_Visual", float alpha = 0.5f)
            {
                var copy = new GameObject(g.name + "_Visual");
                copy.hideFlags = hideFlags;
                copy.layer = 2; //Default Ignore Raycast
                copy.transform.localScale = g.transform.lossyScale;
                copy.transform.SetParent(parent);
                copy.transform.position = g.transform.position;
                copy.transform.rotation = g.transform.rotation;
                if (g.TryGetComponent<Renderer>(out var renderer))
                {
                    UnityEditorInternal.ComponentUtility.CopyComponent(renderer);
                    UnityEditorInternal.ComponentUtility.PasteComponentAsNew(copy);
                    if (alpha < 1)
                    {
                        var newRenderer = copy.GetComponent<Renderer>();
                        SetMaterialsTransparent(newRenderer, alpha);
                    }
                }
                if (g.TryGetComponent<MeshFilter>(out var meshFilter))
                {
                    UnityEditorInternal.ComponentUtility.CopyComponent(meshFilter);
                    UnityEditorInternal.ComponentUtility.PasteComponentAsNew(copy);
                }
                return copy;
            }

            foreach (var g in SelectedGameObjects)
            {
                GameObject parent = CreateVisualCopy(g, visuals.transform);
                parent.transform.localScale = Vector3.one;
                Undo.ClearUndo(parent);
                var renderers = g.GetComponentsInChildren<Renderer>();
                if (renderers.Length > 0)
                {
                    foreach (var r in renderers)
                    {
                        if (r.gameObject != parent)
                        {
                            var copy = CreateVisualCopy(r.gameObject, parent.transform);
                            Undo.ClearUndo(copy);
                        }
                    }
                }
                else
                {
                    var copy = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                    copy.name = g.name + "_Visual";
                    Object.DestroyImmediate(copy.GetComponent<Collider>());
                    copy.hideFlags = hideFlags;
                    copy.layer = 2; //Default Ignore Raycast
                    copy.transform.localScale = Vector3.one * 0.5f;
                    copy.transform.SetParent(parent.transform);
                    copy.transform.position = g.transform.position;
                    copy.transform.rotation = g.transform.rotation;
                    SetMaterialsTransparent(copy.GetComponent<Renderer>(), 0.5f);
                    Undo.ClearUndo(copy);
                }
            }
        }

        public static void EndMove()
        {
            SceneView.duringSceneGui -= OnSceneGUI;
            Object.DestroyImmediate(visuals);
            SelectedGameObjects = null;
            for (int i = instantiatedMaterials.Count - 1; i >= 0; i--)
            {
                Object.DestroyImmediate(instantiatedMaterials[i]);
            }
            instantiatedMaterials.Clear();
        }


        private static void OnSceneGUI(SceneView sceneView)
        {
            if (!IsPerformingMove)
            {
                Debug.LogError($"IsPerformingMove:false when OnSceneView invoked");
                return;
            }

            switch (Event.current.type)
            {
                case EventType.MouseMove:
                    Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
                    RaycastHit hit;

                    if (Physics.Raycast(ray, out hit))
                    {
                        visuals.transform.position = hit.point;
                        visuals.transform.rotation = initialRot * Quaternion.Euler(0, rotation, 0);
                    }
                    break;

                case EventType.ScrollWheel:
                    rotation += Event.current.delta.y;
                    visuals.transform.rotation = initialRot * Quaternion.Euler(0, rotation, 0);
                    Event.current.Use();
                    break;
                case EventType.MouseDown:
                    Event.current.Use();
                    break;
                case EventType.MouseUp:
                    if (Event.current.button == 0)
                    {
                        undoGroup = Undo.GetCurrentGroup();
                        Undo.SetCurrentGroupName("Move To Cursor");
                        try
                        {
                            for (int i = 0; i < SelectedGameObjects.Length; i++)
                            {
                                var visualChild = visuals.transform.GetChild(i);
                                Undo.RecordObject(SelectedGameObjects[i].transform, "Move To Cursor: " + i);
                                SelectedGameObjects[i].transform.position = visualChild.transform.position;
                                SelectedGameObjects[i].transform.rotation = visualChild.transform.rotation;
                            }
                        }
                        finally
                        {
                            Undo.CollapseUndoOperations(undoGroup);
                            EndMove();
                            Event.current.Use();
                        }

                    }
                    else
                    {
                        EndMove();
                    }
                    break;
                case EventType.KeyUp:
                    if (Event.current.keyCode == KeyCode.Escape)
                    {
                        EndMove();
                    }
                    break;
            }
        }
    }
}