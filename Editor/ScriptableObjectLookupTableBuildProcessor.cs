﻿using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEngine;

namespace SMDev.Unitilities
{
    public class ScriptableObjectLookupTableBuildProcessor : IPreprocessBuildWithReport
    {
        public int callbackOrder { get; }

        [MenuItem("Editor Utils/Create\\Validate All Lookup Tables")]
        public static void EnsureAllObjectsUpToDate()
        {
            var subclasses = EditorUtils.GetAssignableFrom<IObjectWithID>();
            HashSet<ObjectLookupTable> validated = new HashSet<ObjectLookupTable>();

            foreach (var subclass in subclasses)
            {
                if(subclass.IsInterface || subclass.IsGenericTypeDefinition)
                {
                    continue;
                }
                var lookup = ObjectLookupTable.GetLookup(subclass);
                if(validated.Contains(lookup)) 
                {
                    continue; 
                }
                LookupTableEditor.FindAll(lookup);
                validated.Add(lookup);
            }
        }

        public void OnPreprocessBuild(BuildReport report)
        {
            EnsureAllObjectsUpToDate();
        }
    }

    public class ScriptableObjectLookupTableAssetPostProcessor : AssetPostprocessor
    {
        static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths, bool didDomainReload)
        {
            foreach (string str in importedAssets)
            {
                var so = AssetDatabase.LoadAssetAtPath<Object>(str);
                if (so != null && so is IObjectWithID soWithId)
                {
                    ObjectLookupTable.GetLookup(soWithId.GetType()).ValidateEntry(so);
                }
            }
        }
        //static void OnWillCreateAsset(string assetName)
        //{
        //    AssetDatabase.asset
        //    var soWithID = AssetDatabase.LoadAssetAtPath<ScriptableObjectWithID>(assetName);
        //    if(soWithID != null)
        //    {
        //        soWithID.GetLookupTable().ValidateEntry(soWithID);
        //        Debug.Log("Validating");
        //    } 
        //    else
        //    {
        //        Debug.Log("Created other");
        //    }
        //    Debug.Log("OnWillCreateAsset is being called with the following asset: " + assetName + ".");
        //}

        //static string[] OnWillSaveAssets(string[] paths)
        //{
        //    Debug.Log("Saving: " + JsonUtility.ToJson(paths));
        //    return paths;
        //}
    }
}