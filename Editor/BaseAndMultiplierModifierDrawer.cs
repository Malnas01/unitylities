﻿using UnityEditor;
using UnityEngine;

namespace SMDev.Unitilities
{
    [CustomPropertyDrawer(typeof(BaseAndMultiplier), true)]
    public class BaseAndMultiplierModifierDrawer : PropertyDrawer
    {
        const float RESET_WIDTH = 45;
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            if (EditorGUIUtility.wideMode) return EditorGUIUtility.singleLineHeight;
            return EditorGUIUtility.singleLineHeight * 2 + EditorGUIUtility.standardVerticalSpacing;
        }
        public override void OnGUI(Rect pos, SerializedProperty property, GUIContent label)
        {
            var baseValue = property.FindPropertyRelative(nameof(BaseAndMultiplier.baseValue));
            var multiplier = property.FindPropertyRelative(nameof(BaseAndMultiplier.multiplier));
            
            if (baseValue.floatValue != 0 || multiplier.floatValue != 1)
            {
                GUI.backgroundColor = Color.blue;
            }

            EditorGUI.LabelField(new Rect(pos.x, pos.y, EditorGUIUtility.labelWidth, pos.height), label);

            var content = new Rect(pos.x + EditorGUIUtility.labelWidth, pos.y, pos.width - EditorGUIUtility.labelWidth - RESET_WIDTH, pos.height);


            var indentLevel = EditorGUI.indentLevel;

            if (EditorGUIUtility.wideMode)
            {
                EditorGUI.indentLevel = 0;
                EditorGUIUtility.labelWidth = 30;

                var baseRect = new Rect(content);
                baseRect.width /= 2.4f;
                EditorGUI.PropertyField(baseRect, baseValue, new GUIContent("Base"));

                content.x += baseRect.width;
                content.width -= baseRect.width;

                EditorGUIUtility.labelWidth = 58;
                EditorGUI.PropertyField(content, multiplier);

            }
            else
            {
                content.height /= 2;
                EditorGUI.indentLevel = 0;
                EditorGUIUtility.labelWidth = 30;

                EditorGUI.PropertyField(content, baseValue, new GUIContent("Base"));

                content.y += content.height;

                EditorGUIUtility.labelWidth = 58;
                EditorGUI.PropertyField(content, multiplier);
            }

            EditorGUI.indentLevel = indentLevel;
            GUI.backgroundColor = Color.white;

            if (GUI.Button(new Rect(pos.x + pos.width - RESET_WIDTH, pos.y, RESET_WIDTH, pos.height), "Reset"))
            {
                baseValue.floatValue = 0;
                multiplier.floatValue = 1;
            }
        }
    }
}