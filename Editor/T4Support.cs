﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;
using UnityEditor;
using UnityEngine;

public class T4Support : AssetPostprocessor
{
    const string T4_SUPPORT_START = "<#//T4SupportStart #>";
    const string T4_SUPPORT_END = "<#//T4SupportEnd #>";

    public static string OnGeneratedSln(string path, string content)
    {
        UnityEngine.Debug.Log("test sln");

        return content;
    }

    static T4? ReadT4FromTTFile(string path)
    {
        if (File.Exists(path))
        {
            using (StreamReader r = new StreamReader(File.OpenRead(path)))
            {
                T4 t4 = new T4();
                t4.path = path;
                var line = r.ReadLine();
                while (line != null && (string.IsNullOrEmpty(t4.nSpace) || string.IsNullOrEmpty(t4.generator) || string.IsNullOrEmpty(t4.lastOutput)))
                {
                    if (line.StartsWith("<#//"))
                    {
                        var split = line.Substring("<#//".Length).Split('=');
                        if (split.Length > 1)
                        {
                            var param = split[0].Trim();
                            var val = split[1].TrimStart();
                            val = val.Substring(0, val.IndexOfAny(new char[] { ' ', '\t', '#' }));
                            if (val.Length > 0)
                            {
                                switch (param)
                                {
                                    case "CustomTool":
                                        t4.generator = val;
                                        break;
                                    case "CustomToolNamespace":
                                        t4.nSpace = val;
                                        break;
                                    case "LastGenOutput":
                                        t4.lastOutput = val;
                                        break;
                                }
                            }
                        }
                    }
                    line = r.ReadLine();
                }
                return t4;
            }
        }
        return null;
    }

    public static string OnGeneratedCSProject(string path, string content)
    {
        var timer = Stopwatch.StartNew();

        Dictionary<string,T4> ttPaths = new Dictionary<string, T4>();
        if (content.Contains(".tt"))
        {
            using (var old = XmlReader.Create(path))
            {
                old.ReadToFollowing("ItemGroup");
                while (old.Read())
                {
                    var p = old.GetAttribute("Include");
                    if (p != null)
                    {
                        if (p.EndsWith(".tt"))
                        {
                            if (!old.IsEmptyElement)
                            {
                                var tt = new T4()
                                {
                                    path = p
                                };
                                var opening = old.LocalName;
                                while (old.NodeType != XmlNodeType.EndElement || old.LocalName != opening)
                                {
                                    old.Read();
                                    switch (old.LocalName)
                                    {
                                        case "CustomToolNamespace":
                                            old.Read();
                                            tt.nSpace = old.ReadContentAsString();
                                            break;
                                        case "LastGenOutput":
                                            old.Read();
                                            tt.lastOutput = old.ReadContentAsString();
                                            break;
                                        case "Generator":
                                            old.Read();
                                            tt.generator = old.ReadContentAsString();
                                            break;
                                    }
                                }

                                ttPaths.Add(tt.path, tt);
                            } 
                        }
                    }
                }
            }
       

            var matches = Regex.Matches(content, @"(?:<None Include="")(.+\.tt)(?:""\s*\/>)");
            foreach(Match m in matches)
            {
                var ttPath = m.Groups[1].Value;

                if(!ttPaths.ContainsKey(ttPath))
                {
                    var tt = ReadT4FromTTFile(ttPath);
                    if(tt != null)
                    {
                        ttPaths.Add(ttPath, tt.Value);
                    }
                }

                if(ttPaths.TryGetValue(ttPath, out var t))
                {
                    //Write data to .tt file
                    var ttContent = File.ReadAllText(t.path);
                    int startIndex = ttContent.IndexOf(T4_SUPPORT_START);
                    int endIndex = ttContent.IndexOf(T4_SUPPORT_END) + T4_SUPPORT_END.Length;

                    if (startIndex < 0 || endIndex < 0)
                    {
                        startIndex = endIndex = 0;
                    }

                    ttContent =
$@"{T4_SUPPORT_START}
<#// CustomTool = {t.generator} #>
<#// CustomToolNamespace = {t.nSpace} #>
<#// LastGenOutput = {t.lastOutput} #>
{T4_SUPPORT_END}

{ttContent.Remove(startIndex, endIndex - startIndex).TrimStart()}
";
                    File.WriteAllText(t.path, ttContent);

                    //Fix .tt file in .csproj
                    content = content.Replace($"<None Include=\"{t.path}\" />",
$@"<Content Include=""{t.path}"" >{(t.nSpace == null ? "" : $@"
        <CustomToolNamespace>{t.nSpace}</CustomToolNamespace>")}
        <Generator>{t.generator}</Generator>
        <LastGenOutput>{t.lastOutput}</LastGenOutput>
</Content>");

                    //Fix output of .tt file in .csproj
                    string outputContentType = "None";
                    if (!string.IsNullOrEmpty(t.lastOutput) && Path.GetExtension(t.lastOutput) == ".cs")
                    {
                        outputContentType = "Compile";
                    }

                    string lastOutputPath = Path.Combine(Path.GetDirectoryName(t.path), t.lastOutput);
                    content = content.Replace($"<{outputContentType} Include=\"{lastOutputPath}\" />",
$@"<{outputContentType} Include=""{lastOutputPath}"" >
        <AutoGen>True</AutoGen>
        <DesignTime>True</DesignTime>
        <DependentUpon>{Path.GetFileName(t.path)}</DependentUpon>
</{outputContentType}>");
                }
            }
        }
        timer.Stop();
        if(ttPaths.Count > 0 ) UnityEngine.Debug.Log($"T4Support: Updated {path} (.tt Count: {ttPaths.Count} Time Elasped {timer.ElapsedMilliseconds}ms)");
        return content;
    }

    struct T4
    {
        public string path;
        public string nSpace;
        public string generator;
        public string lastOutput;
    }
}