﻿using UnityEditor;
using UnityEngine;

namespace SMDev.Unitilities
{
    [CustomPropertyDrawer(typeof(ValueDisplay), true)]
    public class ValueDisplayDrawer : PropertyDrawer
    {
        const float LABEL_WIDTH = 37;
        const float FORMAT_WIDTH = 44;
        const float PADDING = 5;

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            if (EditorGUIUtility.wideMode) return EditorGUIUtility.singleLineHeight;
            return EditorGUIUtility.singleLineHeight * 2 + EditorGUIUtility.standardVerticalSpacing;
        }
        public override void OnGUI(Rect pos, SerializedProperty property, GUIContent label)
        {
            var labelProperty = property.FindPropertyRelative(nameof(ValueDisplay.Label));
            var formatProperty = property.FindPropertyRelative(nameof(ValueDisplay.FormatString));


            EditorGUI.LabelField(new Rect(pos.x, pos.y, EditorGUIUtility.labelWidth, pos.height), label);

            var labelRect = new Rect(pos.x + EditorGUIUtility.labelWidth, pos.y, pos.width - EditorGUIUtility.labelWidth, pos.height);


            var indentLevel = EditorGUI.indentLevel;

            if (EditorGUIUtility.wideMode)
            {
                EditorGUI.indentLevel = 0;
                EditorGUIUtility.labelWidth = FORMAT_WIDTH;

                var formatRect = new Rect(labelRect);
                formatRect.width /= 2.4f;
                EditorGUI.PropertyField(formatRect, formatProperty, new GUIContent("Format"));

                labelRect.x += formatRect.width + PADDING;
                labelRect.width -= formatRect.width + PADDING;

                EditorGUIUtility.labelWidth = LABEL_WIDTH;
                EditorGUI.PropertyField(labelRect, labelProperty);

            }
            else
            {
                labelRect.height /= 2;
                EditorGUI.indentLevel = 0;
                EditorGUIUtility.labelWidth = FORMAT_WIDTH;

                EditorGUI.PropertyField(labelRect, formatProperty, new GUIContent("Format"));

                labelRect.y += labelRect.height;

                EditorGUIUtility.labelWidth = LABEL_WIDTH;
                EditorGUI.PropertyField(labelRect, labelProperty);
            }

            EditorGUI.indentLevel = indentLevel;

        }
    }
}