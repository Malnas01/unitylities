﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using UnityEngine;
namespace SMDev.Unitilities
{

#if UNITY_EDITOR && NET_4_6

    public partial class ContextMenuAttributeGenerator
    {
        public MethodInfo[] contextMethods;

        public ContextMenuAttributeGenerator()
        {
            contextMethods = UnityEditor.TypeCache.GetMethodsWithAttribute<ContextualMenuItemAttribute>().ToArray();
        }

        [UnityEditor.MenuItem("Code Generation/Generate Context Menus")]
        public static void GenerateLayers()
        {
            Generate("Scripts/Generated/Editor/ContextMenuItems");
        }

        private static void Generate(string name)
        {
            // Build the generator with the class name and data source.
            ContextMenuAttributeGenerator generator = new ContextMenuAttributeGenerator();

            // Generate output (class definition).
            string classDefintion = generator.TransformText();

            var outputPath = Path.Combine(Application.dataPath, name + ".cs");

            try
            {
                EditorUtils.EnsureAssetPathExists(outputPath);
                // Save new class to assets folder.
                File.WriteAllText(outputPath, classDefintion);

                // Refresh assets.
                UnityEditor.AssetDatabase.Refresh();
            }
            catch (Exception e)
            {
                Debug.Log("An error occurred while saving file: " + e);
            }
        }
    }
#endif
    
}
