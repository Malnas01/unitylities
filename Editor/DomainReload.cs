using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;

namespace SMDev.Unitilities
{

    public static class PlayModeReloader
    {
        [MenuItem("Editor Utils/Reload/Domain Reload", true)]
        public static bool CanDomainReload() => !EditorApplication.isCompiling;

        [MenuItem("Editor Utils/Reload/Domain Reload")]
        public static void ReloadDomain()
        {
            if (CanDomainReload())
            {
                EditorUtility.RequestScriptReload();
            }
        }

        [MenuItem("Editor Utils/Reload/Scene Reload", true)]
        public static bool CanSceneReload() => EditorApplication.isPlaying;

        [MenuItem("Editor Utils/Reload/Scene Reload")]
        public static void ReloadScene()
        {
            if (CanSceneReload())
            {
                var scene = SceneManager.GetActiveScene();
                if (scene != null)
                {
                    var opts = new LoadSceneParameters { };
                    EditorSceneManager.LoadSceneInPlayMode(scene.path, opts);
                }
            }
        }
    }
}