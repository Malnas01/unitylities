﻿using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SMDev.Unitilities
{
    interface IScriptableObjectList
    {
        void Reorder();
        void CreateAsset();
        void DeleteAssetAt(int index);
    }

    public class ScriptableObjectList<T> : ScriptableObject, IScriptableObjectList where T : ScriptableObject, IIndexable 
    {
        [SerializeField]
        [EditInline]
        private List<T> subAssets;

        public T GetItem(int index)
        {
            if(index < 0) return null;
            return subAssets[index];
        }

        public IReadOnlyList<T> Items => subAssets;
        public void Reorder()
        {
            T[] copy = new T[subAssets.Count];
            //subAssets.CopyTo(copy, 0);

            for(int i = subAssets.Count-1;i>=0;i--)
            {
                var a = subAssets[i];
                if(a.Index >= 0 && a.Index < copy.Length && copy[a.Index] == null)
                {
                    copy[a.Index] = a;
                    subAssets.RemoveAt(i);
                }
            }

            int j = copy.Length - 1;
            for (int i = subAssets.Count - 1; i >= 0; i--)
            {
                var a = subAssets[i];
                while (j >= 0 && copy[j] != null) j--;

                if(j >= 0)
                {
                    copy[j] = a;
                    a.Index = j;
                    subAssets.RemoveAt(i);
                }
                else
                {
                    Debug.LogError($"No space for {a}", a);
                }
            }
            subAssets.Clear();
            subAssets.AddRange(copy);

#if UNITY_EDITOR
            EditorUtility.SetDirty(this);
            AssetDatabase.SaveAssetIfDirty(this);
            AssetDatabase.Refresh();
#endif
        }

        protected virtual void OnValidate()
        {
            for(int i = 0; i < subAssets.Count; i++)
            {
                subAssets[i].Index = i;
            }
        }

        public void CreateAsset()
        {
#if UNITY_EDITOR
            var r = CreateInstance<T>();
            r.name = "New Asset";
            r.Index = subAssets.Count;
            AssetDatabase.AddObjectToAsset(r, this);
            subAssets.Add(r);
            OnValidate();
            EditorUtility.SetDirty(this);
            AssetDatabase.SaveAssetIfDirty(this);
            AssetDatabase.Refresh();
#endif
        }

        public void DeleteAssetAt(int index)
        {

#if UNITY_EDITOR
            var r = subAssets[index];
            AssetDatabase.RemoveObjectFromAsset(r);
            DestroyImmediate(r);
            subAssets.RemoveAt(index);
            OnValidate();
            EditorUtility.SetDirty(this);
            AssetDatabase.SaveAssetIfDirty(this);
#endif
        }
    }

#if UNITY_EDITOR

    [CustomEditor(typeof(ScriptableObjectList<>), true)]
    public class ScriptableObjectListEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            IScriptableObjectList resourceList = (IScriptableObjectList)target;

            serializedObject.Update();
            var property = serializedObject.FindProperty("subAssets.Array");
            var count = property.arraySize;

            const float buttonWidth = 20f;

            GUIContent settingsContent = new GUIContent("✘", "Delete Asset");

            EditorGUILayout.BeginVertical();

            GUI.enabled = false;
            EditorGUILayout.ObjectField(new GUIContent("Scriptable Object"), target, target.GetType(), false);
            GUI.enabled = true;

            for (int i = 0; i < count; i++)
            {
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.BeginVertical();
                var r = property.GetArrayElementAtIndex(i);
                if(r != null)
                {
                    EditorGUILayout.PropertyField(r, GUILayout.Width(EditorGUIUtility.currentViewWidth - buttonWidth * 2));
                }

                EditorGUILayout.EndVertical();

                var buttonStyle = new GUIStyle(EditorStyles.miniButtonRight);
                buttonStyle.fontStyle = FontStyle.Bold;

                var oldColor = GUI.backgroundColor;
                GUI.backgroundColor = Color.red;
                if (GUILayout.Button(settingsContent, buttonStyle))
                {
                    resourceList.DeleteAssetAt(i);
                    i--;
                    count--;
                    property = serializedObject.FindProperty("subAssets.Array");
                    Repaint();
                }
                GUI.backgroundColor = oldColor;
                EditorGUILayout.EndHorizontal(); 
            }
            EditorGUILayout.EndVertical();
            if (GUILayout.Button("Create Child"))
            {
                resourceList.CreateAsset();
                Repaint();
            }

            if (GUILayout.Button("Reorder"))
            {
                resourceList.Reorder();
                Repaint();
            }
        }
    }
#endif
}
