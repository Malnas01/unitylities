﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace SMDev.Unitilities
{
    public static class Utils
    {
        public static bool IsBitSet(this in int mask, int bitIndex)
        {
            return (mask &  (1 << bitIndex)) != 0;
        }
        public static void SetBit(this ref int mask, int bitIndex)
        {
            mask = mask | (1 << bitIndex);
        }

        public static void UnsetBit(this ref int mask, int bitIndex)
        {
            mask = mask & ~(1 << bitIndex);
        }

        public static void ToggleBit(this ref int mask, int bitIndex)
        {
            mask = mask ^ (1 << bitIndex);
        }

        public static string ToBinaryString(this in int mask)
        {
            return Convert.ToString(mask, 2).PadLeft(32, '0');
        }

        public static void NullIfDestroyed<T>(ref T obj) where T : class
        {
            if (obj?.Equals(null) ?? false)
            {
                obj = null;
            }
        }
        public static bool IsAlive(this object obj)
        {
            return obj != null && !obj.Equals(null);
        }

        public static bool IsAliveAndActive(object obj)
        {
            return IsAlive(obj) && (obj is Behaviour b && b.isActiveAndEnabled) || (obj is GameObject go && go.activeSelf);
        }

        public static Vector3 TransformPoint(Vector3 point, Vector3 pos, Quaternion rot, Vector3 scale)
        {
            return pos + rot * Vector3.Scale(scale, point);
        }

        public static Vector3 InverseTransformPoint(Vector3 point, Vector3 pos, Quaternion rot, Vector3 scale)
        {
            Vector3 sInv = new Vector3(1 / scale.x, 1 / scale.y, 1 / scale.z);
            return Vector3.Scale(sInv, Quaternion.Inverse(rot) * (point - pos));
        }

        public static Vector3 ClosestPointOnLineSegment(this Vector3 point, Vector3 lineStart, Vector3 lineEnd)
        {
            Vector3 lineDir = lineEnd - lineStart;
            float length = lineDir.magnitude;
            lineDir.Normalize();
            float projectLength = Mathf.Clamp(Vector3.Dot(point - lineStart, lineDir), 0, length);
            return lineStart + lineDir * projectLength;
        }
        public static Vector3 ClosestPointOnRay(this Vector3 point, Ray ray)
        {
            return ray.origin + Vector3.Project(point - ray.origin, ray.direction);
        }
        public static Vector3 ClosestPointOnRay(this Vector3 point, Vector3 lineStart, Vector3 lineEnd)
        {
            return lineStart + Vector3.Project(point - lineStart, lineEnd - lineStart);
        }
        public static float FrameIndependentLerpAngle(float a, float b, float decay, float deltaTime)
        {
            return Mathf.LerpAngle(b, a, Mathf.Exp(-decay * deltaTime));
        }
        public static float FrameIndependentLerp(float a, float b, float decay, float deltaTime)
        {
            return Mathf.Lerp(b, a, Mathf.Exp(-decay * deltaTime));
        }

        public static Vector3 FrameIndependentLerp(Vector3 a, Vector3 b, float decay, float deltaTime)
        {
            return Vector3.Lerp(b, a, Mathf.Exp(-decay * deltaTime));
        }

        public static Vector3 FrameIndependentSlerp(Vector3 a, Vector3 b, float decay, float deltaTime)
        {
            return Vector3.Slerp(b, a, Mathf.Exp(-decay * deltaTime));
        }

        public static Quaternion FrameIndependentSlerp(Quaternion a, Quaternion b, float decay, float deltaTime)
        {
            return Quaternion.Slerp(b, a, Mathf.Exp(-decay * deltaTime));
        }

        public static float DecayConstantFromHalfLife(float halfLife) => Mathf.Log(2) / halfLife;

        public static int ChooseIndexWeighted(int count, Func<int, float> weight) => ChooseIndexWeighted(count, weight, UnityEngine.Random.value);
        public static int ChooseIndexWeighted(int count, Func<int, float> weight, float randomValueBetween0And1)
        {
            float sum = 0;
            for (int i = 0; i < count; i++)
            {
                sum += weight(i);
            }
            var ran = randomValueBetween0And1 * sum;

            for (int i = 0; i < count; i++)
            {
                ran -= weight(i);
                if (ran <= 0)
                {
                    return i;
                }
            }

            return count - 1;
        }

        static bool IsNull(UnityEngine.Object o) => o == null;
        public static void ClearDestroyedObjects<T>(this List<T> gameObjects) where T : UnityEngine.Object
        {
            ClearObjects(gameObjects, IsNull);
        }

        public static void ClearObjects<T>(this List<T> objects, Func<T, bool> shouldClear)
        {
            for (int i = objects.Count - 1; i >= 0; i--)
            {
                if (shouldClear(objects[i]))
                {
                    objects.RemoveAt(i);
                }
            }
        }

        //public static void EnsureElementCount<T>(this T list, int count, Action<T> addMethod, Action<T> removeMethod) where T : ICollection<T>
        //{
        //    while (list.Count < count) addMethod(list);

        //    while (list.Count > count) removeMethod(list);
        //}
        public static void EnsureElementCount<T>(this IList<T> list, int count, Func<T> createMethod, Action<T> destroyMethod)
        {
            while (list.Count < count) list.Add(createMethod());

            while (list.Count > count)
            {
                var index = list.Count - 1;
                destroyMethod(list[index]);
                list.RemoveAt(index);
            }
        }
    }
}
