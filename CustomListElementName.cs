﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SMDev.Unitilities
{
    public class CustomListElementName : PropertyAttribute 
    {
        public string FieldMethodOrPropertyName;
        public string Text = "{1}";
        public bool PassIndexToMethodCall = true;
        public int ParentTraversalHeight = 0;
        public CustomListElementName(string PropertyPath, string Text = "{1}", int parentTraversalHeight = 0, bool passIndexToMethodCall = true)
        {
            this.FieldMethodOrPropertyName = PropertyPath;
            this.Text = Text;
            this.PassIndexToMethodCall = passIndexToMethodCall;
            this.ParentTraversalHeight = parentTraversalHeight;
        }

#if UNITY_EDITOR
        public string GetName(SerializedProperty property, int? indexOverride = null)
        {
            object propertyObject = null;

            string strindex = "";
            int intdex = -1;

            if(indexOverride.HasValue)
            {
                strindex = indexOverride.Value.ToString();
                intdex = indexOverride.Value;
            }
            else if (property.propertyPath.EndsWith(']'))
            {
                var lastIndex = property.propertyPath.LastIndexOf('[') + 1;
                strindex = property.propertyPath.Substring(lastIndex, property.propertyPath.Length - lastIndex - 1);
                intdex = int.Parse(strindex);
            }

            if (!string.IsNullOrEmpty(FieldMethodOrPropertyName))
            {
                object elementObject = null;
                property.TraverseSerializedPropertyPath(out _, out elementObject, out var elementType, ParentTraversalHeight);
                if (elementObject != null)
                {
                    propertyObject = EditorUtils.GetFieldPropertyOrReturnValue(elementObject, FieldMethodOrPropertyName, methodArgs: PassIndexToMethodCall ? new object[] { intdex } : null);
                }
            }

            string text = string.Format(Text, propertyObject, strindex);

            //If no name was found default to displayName
            if (string.IsNullOrEmpty(text))
            {
                text = $"{property.displayName}";
            }
            return text;
        }
#endif

    }

#if UNITY_EDITOR


    [CustomPropertyDrawer(typeof(CustomListElementName), true)]
    public class ProbabilityListDrawer : PropertyDrawer
    {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUI.GetPropertyHeight(property);
        }
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            CustomListElementName customName = attribute as CustomListElementName;

            if (customName != null)
            {
                label = new GUIContent(customName.GetName(property));
            }

            EditorGUI.PropertyField(position, property, label);
        }
    }
#endif
}