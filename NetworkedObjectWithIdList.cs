﻿#if NGO
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Netcode;

namespace SMDev.Unitilities
{
    [System.Serializable]
    public class NetworkedObjectWithIdList<T> : NetworkList<int>, IEnumerable<T> where T : class, IObjectWithID
    {
        List<T> _values = new List<T>();
        ObjectLookupTable _lookup;
        public NetworkedObjectWithIdList() : base()
        {
            OnListChanged += NetworkedObjectWithIdList_OnListChanged;
        }
        public NetworkedObjectWithIdList(IEnumerable<T> values = default,
            NetworkVariableReadPermission readPerm = DefaultReadPerm,
            NetworkVariableWritePermission writePerm = DefaultWritePerm)
            : base(values.Select(x=>x.Id), readPerm, writePerm)
        {
            OnListChanged += NetworkedObjectWithIdList_OnListChanged;
        }
        private void NetworkedObjectWithIdList_OnListChanged(NetworkListEvent<int> changeEvent)
        {
            if(_lookup == null)
            {
                _lookup = ObjectLookupTable.GetLookup<T>();
            }
            switch (changeEvent.Type)
            {
                case NetworkListEvent<int>.EventType.Add:
                    _values.Add(_lookup[changeEvent.Value] as T);
                    break;
                case NetworkListEvent<int>.EventType.Insert:
                    _values.Insert(changeEvent.Index, _lookup[changeEvent.Value] as T);
                    break;
                case NetworkListEvent<int>.EventType.Remove:
                    _values.Remove(_lookup[changeEvent.Value] as T);
                    break;
                case NetworkListEvent<int>.EventType.RemoveAt:
                    _values.RemoveAt(changeEvent.Index);
                    break;
                case NetworkListEvent<int>.EventType.Value:
                    _values[changeEvent.Index] = _lookup[changeEvent.Value] as T;
                    break;
                case NetworkListEvent<int>.EventType.Clear:
                    _values.Clear();
                    break;
                case NetworkListEvent<int>.EventType.Full:
                    _values.Clear();
                    _values.Capacity = Count;
                    foreach(int item in (NetworkList<int>)this)
                    {
                        _values.Add(_lookup[item] as T);
                    }
                    break;
            }
        }

        /// <inheritdoc />
        public void Add(T item) => Add(item.Id);    

        /// <inheritdoc />
        public bool Contains(T item) => Contains(item.Id);

        /// <inheritdoc />
        public bool Remove(T item) => Remove(item.Id);

        /// <inheritdoc />
        public int IndexOf(T item) => IndexOf(item.Id);

        /// <inheritdoc />
        public void Insert(int index, T item) => Insert(index, item.Id);

        public void Set(int index, T item) => this[index] = item.Id;

        public T Get(int index) => _values[index];

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return _values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _values.GetEnumerator();
        }
    }
}
#endif