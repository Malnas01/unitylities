﻿#if LOCALIZATION
using UnityEngine.Localization;
using UnityEngine.Localization.Settings;

namespace SMDev.Unitilities
{
    public static class LocalizationHelper
    {
        public static void Ensure(ref LocalizedString localizedString, string key, string value, bool isSmart = false, string tableName = "Gameplay UI")
        {
            if (localizedString.IsEmpty)
            {
                localizedString = LocalizationHelper.MakeLocalizedString(key, value, isSmart, tableName);
            }
        }
        public static LocalizedString MakeLocalizedString(string key, string value, bool isSmart = false, string tableName = "Gameplay UI")
        {
            var table = LocalizationSettings.StringDatabase.GetTable(tableName, LocalizationSettings.AvailableLocales.GetLocale("en"));

            if(table == null)
            {
                return new LocalizedString();
            }
            var id = table.SharedData.GetId(key, true);

            if (!table.TryGetValue(id, out var entry))
            {
                table.AddEntry(id, value);
                if(table.TryGetValue(id, out entry))
                {
                    entry.IsSmart = isSmart;
                }
#if UNITY_EDITOR
                UnityEditor.EditorUtility.SetDirty(table);
                UnityEditor.EditorUtility.SetDirty(table.SharedData);
#endif
            }
            return new LocalizedString("Gameplay UI", id);
        }
    }
}
#endif