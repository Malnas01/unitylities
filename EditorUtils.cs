﻿using UnityEngine;
using System.Collections.Generic;
using System;
using Debug = UnityEngine.Debug;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using System.Linq;
using Object = UnityEngine.Object;
using System.IO;
using System.Reflection;

#if UNITY_EDITOR
using UnityEditor.SceneManagement;
using UnityEditor;
#endif

namespace SMDev.Unitilities
{
#if UNITY_EDITOR

    //https://discussions.unity.com/t/locate-custompropertydrawer-from-serializedobject/659175
    public class PropertyHandler
    {

        private static MethodInfo getHandler;
        private static object[] getHandlerParams;

        private object handler;
        private Type type;

        private PropertyInfo propertyDrawerInfo;
        private MethodInfo guiHandler;
        private object[] guiParams;

        public PropertyDrawer propertyDrawer
        {

            get { return propertyDrawerInfo.GetValue(handler, null) as PropertyDrawer; }
        }

        static PropertyHandler()
        {

            getHandler = Type.GetType("UnityEditor.ScriptAttributeUtility, UnityEditor").GetMethod("GetHandler", BindingFlags.NonPublic | BindingFlags.Static);
            getHandlerParams = new object[1];
        }

        private PropertyHandler(object handler)
        {

            this.handler = handler;

            type = handler.GetType();
            propertyDrawerInfo = type.GetProperty("propertyDrawer", BindingFlags.NonPublic | BindingFlags.Instance);
            guiHandler = type.GetMethod("OnGUI", BindingFlags.Public | BindingFlags.Instance);
            guiParams = new object[4];
        }

        public bool OnGUI(Rect position, SerializedProperty property, GUIContent label, bool includeChildren)
        {

            guiParams[0] = position;
            guiParams[1] = property;
            guiParams[2] = label;
            guiParams[3] = includeChildren;

            return (bool)guiHandler.Invoke(handler, guiParams);
        }

        public static PropertyHandler GetHandler(SerializedProperty property)
        {

            getHandlerParams[0] = property;

            return new PropertyHandler(getHandler.Invoke(null, getHandlerParams));
        }
    }
    public static partial class EditorUtils
    {
        //https://stackoverflow.com/questions/49920863/bindingflags-get-all-of-them-maximum-members
        public const BindingFlags AllBindingFlags = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic; // (BindingFlags)(60);
        public static bool HasBeenDisposed(this SerializedObject serializedObject)
        {
            try
            {
                var id = serializedObject.targetObject.GetInstanceID();
                return id == 0;
            }
            catch
            {
                return true;
            }
        }

        public static Assembly GetLoadedAssemblyFromName(string shortName)
        {
            return AppDomain.CurrentDomain.GetAssemblies().FirstOrDefault(a => a.GetName().Name == shortName);
        }
        public static IEnumerable<Type> GetSubclasses(Type type, Assembly assembly = null)
        {
            if(assembly == null)
            {
                return AppDomain.CurrentDomain.GetAssemblies().SelectMany(x => x.GetTypes()
               .Where(t => t.IsSubclassOf(type)));
            }
            return assembly.GetTypes()
               .Where(t => t.IsSubclassOf(type));
        }
        public static IEnumerable<Type> GetSubclasses<T>() => GetSubclasses(typeof(T));

        public static IEnumerable<Type> GetAssignableFrom(Type type)
        {
            return AppDomain.CurrentDomain.GetAssemblies().SelectMany(x=> x.GetTypes()
               .Where(t => type.IsAssignableFrom(t)));
        }
        public static IEnumerable<Type> GetAssignableFrom<T>() => GetAssignableFrom(typeof(T));

        public static bool SelectionHasComponent<T>()
        {
            return Selection.gameObjects.All(go => go.GetComponent<T>() != null);
        }
        public static bool IsPrefabRoot(GameObject go)
        {
            return PrefabUtility.IsAnyPrefabInstanceRoot(go) || (PrefabUtility.GetPrefabAssetType(go) != PrefabAssetType.NotAPrefab && PrefabUtility.GetPrefabInstanceStatus(go) == PrefabInstanceStatus.NotAPrefab);
        }

        /// <summary>
        /// Returns attributes of type <typeparamref name="TAttribute"/> on <paramref name="serializedProperty"/>.
        /// </summary>
        public static TAttribute[] GetAttributes<TAttribute>(this SerializedProperty serializedProperty, bool inherit, out FieldInfo fieldInfo, out object objectValue, out Type type) where TAttribute : Attribute
        {
            if (serializedProperty == null)
            {
                throw new ArgumentNullException(nameof(serializedProperty));
            }

            var targetObjectType = serializedProperty.serializedObject.targetObject.GetType();

            if (targetObjectType == null)
            {
                throw new ArgumentException($"Could not find the {nameof(targetObjectType)} of {nameof(serializedProperty)}");
            }

            TraverseSerializedPropertyPath(serializedProperty, out fieldInfo, out objectValue, out type);
            return (TAttribute[])fieldInfo.GetCustomAttributes<TAttribute>(inherit);

            //foreach (var pathSegment in serializedProperty.propertyPath.Split('.'))
            //{
            //    var fieldInfo = targetObjectType.GetField(pathSegment, AllBindingFlags);
            //    if (fieldInfo != null)
            //    {
            //        return (TAttribute[])fieldInfo.GetCustomAttributes<TAttribute>(inherit);
            //    }

            //    var propertyInfo = targetObjectType.GetProperty(pathSegment, AllBindingFlags);
            //    if (propertyInfo != null)
            //    {
            //        return (TAttribute[])propertyInfo.GetCustomAttributes<TAttribute>(inherit);
            //    }

            //    //Recurse up parents to check private fields within base type
            //    var t = targetObjectType.BaseType;
            //    while (t != null)
            //    {
            //        fieldInfo = targetObjectType.GetField(pathSegment, AllBindingFlags);
            //        if (fieldInfo != null)
            //        {
            //            return (TAttribute[])fieldInfo.GetCustomAttributes<TAttribute>(inherit);
            //        }

            //        propertyInfo = targetObjectType.GetProperty(pathSegment, AllBindingFlags);
            //        if (propertyInfo != null)
            //        {
            //            return (TAttribute[])propertyInfo.GetCustomAttributes<TAttribute>(inherit);
            //        }
            //        t = t.BaseType;
            //    }
            //}

            //return new TAttribute[0];
            //throw new ArgumentException($"Could not find the field or property of {nameof(serializedProperty)}");
        }


        public static TAttribute[] GetAttributes<TAttribute>(this SerializedProperty serializedProperty, bool inherit) where TAttribute : Attribute => GetAttributes<TAttribute>(serializedProperty, inherit, out _, out _, out _);
        //{
        //    if (serializedProperty == null)
        //    {
        //        throw new ArgumentNullException(nameof(serializedProperty));
        //    }

        //    var targetObjectType = serializedProperty.serializedObject.targetObject.GetType();

        //    if (targetObjectType == null)
        //    {
        //        throw new ArgumentException($"Could not find the {nameof(targetObjectType)} of {nameof(serializedProperty)}");
        //    }

        //    TraverseSerializedPropertyPath(serializedProperty, out FieldInfo fieldInfo, out object objectValue, out Type type);
        //    return (TAttribute[])fieldInfo.GetCustomAttributes<TAttribute>(inherit);

        //}

        [MenuItem("CONTEXT/Animator/Ragdoll/Create Ragdoll", true, 2000)]
        static bool CanSetRagdollBones(MenuCommand command)
        {
            Animator animator = (command.context as Animator);
            return animator.isHuman;
        }

        [MenuItem("CONTEXT/Animator/Ragdoll/Create Ragdoll", false, 2000)]
        static void SetRagdollBones(MenuCommand command)
        {
            Animator animator = (command.context as Animator);
            var type = typeof(ScriptableWizard).Assembly.GetType("UnityEditor.RagdollBuilder");
            object wizard = ScriptableWizard.DisplayWizard("Ragdoll Builder", type);

            type.GetField("pelvis").SetValue(wizard, animator.GetBoneTransform(HumanBodyBones.Hips));
            type.GetField("leftHips").SetValue(wizard, animator.GetBoneTransform(HumanBodyBones.LeftUpperLeg));
            type.GetField("leftKnee").SetValue(wizard, animator.GetBoneTransform(HumanBodyBones.LeftLowerLeg));
            type.GetField("leftFoot").SetValue(wizard, animator.GetBoneTransform(HumanBodyBones.LeftFoot));
            type.GetField("rightHips").SetValue(wizard, animator.GetBoneTransform(HumanBodyBones.RightUpperLeg));
            type.GetField("rightKnee").SetValue(wizard, animator.GetBoneTransform(HumanBodyBones.RightLowerLeg));
            type.GetField("rightFoot").SetValue(wizard, animator.GetBoneTransform(HumanBodyBones.RightFoot));
            type.GetField("leftArm").SetValue(wizard, animator.GetBoneTransform(HumanBodyBones.LeftUpperArm));
            type.GetField("leftElbow").SetValue(wizard, animator.GetBoneTransform(HumanBodyBones.LeftLowerArm));
            type.GetField("rightArm").SetValue(wizard, animator.GetBoneTransform(HumanBodyBones.RightUpperArm));
            type.GetField("rightElbow").SetValue(wizard, animator.GetBoneTransform(HumanBodyBones.RightLowerArm));
            type.GetField("middleSpine").SetValue(wizard, animator.GetBoneTransform(HumanBodyBones.Spine));
            type.GetField("head").SetValue(wizard, animator.GetBoneTransform(HumanBodyBones.Head));
        }

        public static Transform FindOrAddChildWithName(this Transform parent, string name)
        {
            var child = parent.Find(name);
            if (child == null)
            {
                child = new GameObject(name).transform;
                child.transform.SetParent(parent, false);
            }
            return child;
        }

        public static T FindOrAddChildWithName<T>(this Transform parent, string name) where T : Component
        {
            var child = FindOrAddChildWithName(parent, name);
            var component = child.GetComponent<T>();
            if (component == null)
            {
                component = child.gameObject.AddComponent<T>();
            }
            return component;
        }


        [MenuItem("GameObject/Hide Flags/Show Hidden Components")]
        private static void ShowHiddenComponents(MenuCommand command)
        {
            ShowHiddenComponents(command.context as GameObject);
        }

        static void ShowHiddenComponents(GameObject gameObject)
        {
            var comps = gameObject.GetComponents<Component>();
            foreach (var comp in comps)
            {
                switch (comp.hideFlags)
                {
                    case HideFlags.HideAndDontSave:
                        comp.hideFlags = HideFlags.DontSave;
                        break;
                    case HideFlags.HideInHierarchy:
                    case HideFlags.HideInInspector:
                        comp.hideFlags = HideFlags.None;
                        break;
                }
            }
        }

        [MenuItem("Help/Hide Flags/Show All Objects")]
        private static void ShowAll()
        {
            var allGameObjects = Object.FindObjectsOfType<GameObject>();
            foreach (var go in allGameObjects)
            {
                switch (go.hideFlags)
                {
                    case HideFlags.HideAndDontSave:
                        go.hideFlags = HideFlags.DontSave;
                        break;
                    case HideFlags.HideInHierarchy:
                    case HideFlags.HideInInspector:
                        go.hideFlags = HideFlags.None;
                        break;
                }
                ShowHiddenComponents(go);
            }
        }

        public static void SaveAsset(Object asset)
        {
            EditorUtility.SetDirty(asset);
            AssetDatabase.SaveAssetIfDirty(asset);
        }

        static List<SerializedObject> soCache = new List<SerializedObject>();
        public static void RunPerMultipleDifferentValue(this SerializedProperty property, Action<SerializedProperty> action) => RunPerMultipleDifferentValue(property, action, soCache);
        public static void RunPerMultipleDifferentValue(this SerializedProperty property, Action<SerializedProperty> action, List<SerializedObject> soCache)
        {
            if (!property.hasMultipleDifferentValues)
            {
                action(property);
            }
            else
            {
                int objectCount = property.serializedObject.targetObjects.Length;
                void BuildSOCache()
                {
                    foreach (var so in soCache)
                    {
                        so.Dispose();
                    }
                    soCache.Clear();
                    for (int i = 0; i < objectCount; i++)
                    {
                        // Get the property on the corresponding serializedObject
                        var o = property.serializedObject.targetObjects[i];
                        soCache.Add(new SerializedObject(o));
                    }
                }

                if (soCache.Count != objectCount)
                {
                    BuildSOCache();
                }

                for (int i = objectCount - 1; i >= 0; i--)
                {
                    if (soCache[i].targetObject != property.serializedObject.targetObjects[i])
                    {
                        BuildSOCache();
                    }
                    var prop = soCache[i].FindProperty(property.propertyPath);
                    action(prop);
                }
            }
        }

        public static object GetFieldPropertyOrReturnValue(object obj, string memberName, BindingFlags bindingFlags = AllBindingFlags, object[] methodArgs = null)
        {
            object returnValue = null;

            var type = obj.GetType();
            var fieldInfo = type.GetField(memberName, bindingFlags);
            if(fieldInfo != null)
            {
                returnValue = fieldInfo.GetValue(obj);
            }

            if(returnValue == null)
            {
                var propertyInfo = type.GetProperty(memberName, bindingFlags);
                if(propertyInfo != null)
                {
                    try
                    {
                        returnValue = propertyInfo.GetValue(obj);
                    } 
                    catch { }
                }
            }

            if(returnValue == null)
            {
                var methodInfo = type.GetMethod(memberName, bindingFlags);
                if(methodInfo != null)
                {
                    try
                    {
                        returnValue = methodInfo.Invoke(obj, methodArgs);
                    }
                    catch { }
                }
            }
            return returnValue;

        }

        public static object GetParentObject(this SerializedProperty property, out Type parentType)
        {
            TraverseSerializedPropertyPath(property, out _, out var objectValue, out parentType, 1);
            return objectValue;
            //object parentObject = property.serializedObject.targetObject;
            //parentType = parentObject.GetType();
            //System.Reflection.FieldInfo fi = null;

            //string[] array = property.propertyPath.Split('.');
            //for (int i = 0; i < array.Length-1; i++)
            //{
            //    string name = array[i];
            //    if (name == "Array")
            //    {
            //        var index = int.Parse(array[i + 1]["data[".Length..^1]);

            //        if (parentType.IsArray)
            //        {
            //            parentObject = (parentObject as Array).GetValue(index);
            //            parentType = parentObject?.GetType() ?? parentType.GetElementType();
            //        }
            //        else
            //        {
            //            var indexor = parentType.GetMethod("get_Item");
            //            parentObject = indexor.Invoke(parentObject, new object[] { index });
            //            parentType = parentObject?.GetType() ?? indexor.ReturnType;
            //        }

            //        i++;
            //    }
            //    else
            //    {
            //        fi = parentType.GetField(name, AllBindingFlags);
            //        parentObject = fi.GetValue(parentObject);
            //        parentType = parentObject?.GetType() ?? fi.FieldType;
            //    }
            //}

            //return parentObject;
        }

        public static void TraverseSerializedPropertyPath(this SerializedProperty property, out FieldInfo fieldInfo, out object objectValue, out System.Type declaredType, int stopEarlyDepth = 0)
        {
            objectValue = property.serializedObject.targetObject;
            declaredType = objectValue.GetType();
            fieldInfo = null;

            string[] array = property.propertyPath.Split('.');

            int length = array.Length - stopEarlyDepth;

            for (int i = 0; i < length; i++)
            {
                string name = array[i];
                if (name == "Array")
                {
                    i++;
                    if(i >= length)
                    {
                        return;
                    }
                    var index = int.Parse(array[i]["data[".Length..^1]);

                    if (declaredType.IsArray)
                    {
                        objectValue = (objectValue as Array).GetValue(index);
                        declaredType = objectValue?.GetType() ?? declaredType.GetElementType();
                    }
                    else
                    {
                        var indexor = declaredType.GetMethod("get_Item");
                        objectValue = indexor.Invoke(objectValue, new object[] { index });
                        declaredType = objectValue?.GetType() ?? indexor.ReturnType;
                    }

                }
                else
                {
                    fieldInfo = declaredType.GetField(name, AllBindingFlags);
                    objectValue = fieldInfo.GetValue(objectValue);
                    declaredType = objectValue?.GetType() ?? fieldInfo.FieldType;
                }
            }
        }

        public static System.Type GetTypeFromSerializedProperty(this SerializedProperty property, bool declaredType = false)
        {
            TraverseSerializedPropertyPath(property, out _, out var objectValue, out var type);
            return declaredType ? type : objectValue.GetType();
            //object parentObject = property.serializedObject.targetObject;
            //System.Type parentType = parentObject.GetType();
            //System.Reflection.FieldInfo fi = null;

            //string[] array = property.propertyPath.Split('.');
            //for (int i = 0; i < array.Length; i++)
            //{
            //    string name = array[i];
            //    if (name == "Array")
            //    {
            //        var index = int.Parse(array[i + 1]["data[".Length..^1]);
            //        var indexor = parentType.GetMethod("get_Item");
            //        parentObject = indexor.Invoke(parentObject, new object[] { index });
            //        parentType = indexor.ReturnType;
            //        i++;
            //    }
            //    else
            //    {
            //        fi = parentType.GetField(name, AllBindingFlags);
            //        parentObject = fi.GetValue(parentObject);
            //        parentType = fi.FieldType;
            //    }
            //}
            //return parentType;
        }

        public static void EnsureAssetPathExists(string path)
        {
            int assetsIndex = path.IndexOf("Assets");
            if (assetsIndex < 0)
            {
                Debug.LogError("Invalid asset path " + path);
                return;
            }
            else
            {
                path = path.Substring(assetsIndex);
            }
            path = path.TrimEnd('/', '\\');

            if (Path.HasExtension(path))
            {
                path = Path.GetDirectoryName(path);
            }

            if (AssetDatabase.IsValidFolder(path))
            {
                return;
            }
            else
            {
                var parentPath = Path.GetDirectoryName(path);
                EnsureAssetPathExists(parentPath);
                var folderName = Path.GetFileName(path);
                var success = AssetDatabase.CreateFolder(parentPath, folderName);
            }
        }



        public static void SearchAllScenes(Func<GameObject, bool> runOnRootObjects)
        {
            var scenes = from guid in AssetDatabase.FindAssets("t:Scene", new string[] { "Assets" })
                         select AssetDatabase.GUIDToAssetPath(guid);

            foreach (var s in scenes)
            {
                try
                {
                    Scene scene = default;
                    bool open = false;
                    for (int i = 0; i < EditorSceneManager.sceneCount; i++)
                    {
                        scene = EditorSceneManager.GetSceneAt(i);
                        if (scene.path == s)
                        {
                            open = true;
                            break;
                        }
                    }

                    if (!open) scene = EditorSceneManager.OpenScene(s, OpenSceneMode.Additive);
                    var rootObjects = scene.GetRootGameObjects();
                    List<string> references = new List<string>();
                    foreach (var r in rootObjects)
                    {
                        if (!runOnRootObjects(r))
                        {
                            break;
                        }
                    }
                    if (!open) EditorSceneManager.CloseScene(scene, true);
                }
                catch (Exception)
                {

                }
            }
        }



        [MenuItem("Assets/Show References In Project")]
        static void ListReferencesInProject()
        {
            if (Selection.activeObject != null)
            {
                var path = AssetDatabase.GetAssetPath(Selection.activeObject);
                if (!string.IsNullOrEmpty(path))
                {
                    List<UnityEngine.Object> foundObjects = new List<Object>();

                    var objectsWithReference = FindProjectReferences(Selection.activeObject);
                    //from guid in AssetDatabase.FindAssets("ref:" + path, new string[] { "Assets" })
                    //                           select AssetDatabase.GUIDToAssetPath(guid);

                    foreach (var o in objectsWithReference)
                    {
                        var obj = AssetDatabase.LoadAssetAtPath<Object>(o);
                        foundObjects.Add(obj);
                        Debug.Log("Asset: " + o, obj);
                    }

                    Dictionary<string, List<string>> references = new Dictionary<string, List<string>>();

                    void AddObjectReference(UnityEngine.Object uObject)
                    {
                        if (uObject == null) return;

                        string name = uObject.ToString();
                        Transform current = null;
                        string path = "";
                        if (uObject is GameObject go)
                        {
                            current = go.transform;
                            path = go.scene.path;
                        }
                        else if (uObject is Component component)
                        {
                            current = component.transform;
                            path = component.gameObject.scene.path;
                            //name = $"{uObject.ToString()}()";
                        }
                        else
                        {
                            path = AssetDatabase.GetAssetPath(uObject);
                        }

                        while (current?.transform.parent != null)
                        {
                            current = current.transform.parent;
                            name = current.name + "->" + name;
                        }

                        if (!references.ContainsKey(path))
                        {
                            references.Add(path, new List<string>());
                        }
                        references[path].Add(name);
                    }

                    SearchAllScenes((GameObject rootObject) =>
                    {
                        AddObjectReference(GameObjectGetReferenceingComponent(rootObject, Selection.activeObject));
                        IterateChildren(rootObject, (child) => AddObjectReference(GameObjectGetReferenceingComponent(child, Selection.activeObject)), true);
                        return true;
                    });

                    foreach (var reference in references)
                    {
                        var log = $"Scene - {reference.Key}:\n";
                        foreach (var name in reference.Value)
                        {
                            log += "\"" + name + "\"\n";
                        }
                        var asset = AssetDatabase.LoadAssetAtPath<SceneAsset>(reference.Key);
                        foundObjects.Add(asset);
                        Debug.Log(log, asset);
                    }
                    if (foundObjects.Count > 0)
                    {
                        EditorGUIUtility.PingObject(foundObjects[0]);
                        Selection.objects = foundObjects.ToArray();
                    }
                }
            }
        }

        /// <summary>
        /// Allows us to copy over persistent actions in UnityEvent from script which show up in inspector.
        /// <remarks>Currently only works for void methods.</remarks>
        /// </summary>
        /// <example>
        /// Copy over event in editor script when button is pressed and save changes.
        /// <code>
        /// CopyUnityEvent(airTapButton.OnTapDown, interactableScriptEvents.OnPress, false);
        /// EditorUtility.SetDirty(interactableScriptEvents);
        /// </code>
        /// </example>
        /// <param name="oldEvent">Old event to copy from</param>
        /// <param name="newEvent">New event to copy over to</param>
        /// <param name="overwrite">Overwrite the existing listeners</param>
        // todo: Add support for figuring out argument types for the methods so we can add non-void listeners. EventTool does have support to add it. Issues: 1. Find correct target MonoBehaviour. 2. Find argument type. 3. Find argument value set in inspector.
        public static void CopyUnityEvent<U>(UnityEngine.Events.UnityEvent oldEvent, UnityEngine.Events.UnityEvent<U> newEvent, bool overwrite = false)
        {
            for (int i = 0; i < oldEvent.GetPersistentEventCount(); i++)
            {
                var methodTarget = oldEvent.GetPersistentTarget(i);
                var methodName = oldEvent.GetPersistentMethodName(i);

                try
                {
                    var execute =
                        Delegate.CreateDelegate(typeof(UnityAction<U>), methodTarget, methodName) as
                            UnityAction<U>;
                    if (overwrite == false)
                    {
                        UnityEditor.Events.UnityEventTools.AddPersistentListener(newEvent, execute);
                    }
                    else
                    {
                        if (i >= newEvent.GetPersistentEventCount())
                        {
                            UnityEditor.Events.UnityEventTools.AddPersistentListener(newEvent, execute);

                        }
                        else
                        {
                            UnityEditor.Events.UnityEventTools.RegisterPersistentListener(newEvent, i, execute);

                        }
                    }
                }
                catch (Exception exp)
                {
                    Debug.LogError(
                        $"Could not bind method: {methodName}. Probably because it isn't a void method. Exceptions: {exp}");
                    continue;
                }
            }
        }

        [MenuItem("Assets/Force Reserialize Related Assets")]
        static void ForceReserializeAssets()
        {
            if (Selection.activeObject != null)
            {
                var path = AssetDatabase.GetAssetPath(Selection.activeObject);
                if (!string.IsNullOrEmpty(path))
                {
                    var objectsWithReference = FindProjectReferences(Selection.activeObject);
                    //var objectsWithReference = from guid in AssetDatabase.FindAssets("ref:" + path, new string[] { "Assets" })
                    //                           select AssetDatabase.GUIDToAssetPath(guid);
                    AssetDatabase.ForceReserializeAssets(objectsWithReference);

                    if (Selection.activeObject is MonoScript monoScript)
                    {
                        var scenes = from guid in AssetDatabase.FindAssets("t:Scene", new string[] { "Assets" })
                                     select AssetDatabase.GUIDToAssetPath(guid);

                        List<string> openScenes = new List<string>(EditorSceneManager.sceneCount);
                        for (int i = 0; i < EditorSceneManager.sceneCount; i++)
                        {
                            openScenes.Add(EditorSceneManager.GetSceneAt(i).path);
                        }

                        SearchAllScenes((rootObject) =>
                        {
                            if (rootObject.GetComponentInChildren(monoScript.GetClass()) != null)
                            {
                                if (openScenes.Contains(rootObject.scene.path))
                                {
                                    EditorSceneManager.MarkSceneDirty(rootObject.scene);
                                    EditorSceneManager.SaveModifiedScenesIfUserWantsTo(new Scene[] { rootObject.scene });
                                }
                                else
                                {
                                    EditorSceneManager.SaveScene(rootObject.scene);
                                }

                                return false;
                            }
                            return true;
                        });
                    }

                    return;
                }
            }
            if (EditorUtility.DisplayDialog("Reserialize All Assets?", "Are you sure you want to reserialize all assets in the project?", "Yes", "No"))
            {
                AssetDatabase.ForceReserializeAssets();
            }
        }


        public static UnityEngine.Object GameObjectGetReferenceingComponent(GameObject gameObjectToTest, UnityEngine.Object referenceTo)
        {
            if (PrefabUtility.GetPrefabAssetType(gameObjectToTest) != PrefabAssetType.NotAPrefab)
            {
                if (PrefabUtility.GetCorrespondingObjectFromSource(gameObjectToTest) == referenceTo)
                {
                    return gameObjectToTest;
                }
            }

            var components = gameObjectToTest.GetComponents<Component>();
            foreach (var comp in components)
            {
                if (comp == null)
                    continue;

                var so = new SerializedObject(comp);
                var sp = so.GetIterator();
                while (sp.NextVisible(true))
                {
                    if (sp.propertyType == SerializedPropertyType.ObjectReference)
                    {
                        if (sp.objectReferenceValue == referenceTo || (sp.objectReferenceValue is Component component && component.gameObject == referenceTo))
                        {
                            return comp;
                        }
                    }
                }
            }
            return null;
        }

        public static IEnumerable<string> FindProjectReferences(Object obj)
        {
            var path = AssetDatabase.GetAssetPath(obj);

            var referenceAssets = AssetDatabase.FindAssets($"ref:\"{path}\"").AsEnumerable();

            if (obj is MonoScript script)
            {
                var search = $"t:\"{script.GetClass().Name}\"";
                var results = AssetDatabase.FindAssets(search);
                referenceAssets = referenceAssets.Concat(results);
            }

            return from guid in referenceAssets select AssetDatabase.GUIDToAssetPath(guid);
        }


        public static IEnumerable<string> FindAssetsOfType(Type type)
        {
            var search = $"t:{type.Name}";
            IEnumerable<string> results = AssetDatabase.FindAssets(search);

            if (results.Count() == 0 && typeof(UnityEngine.Object).IsAssignableFrom(type))
            {
                results = Resources.FindObjectsOfTypeAll(type).Select(x => AssetDatabase.GetAssetPath(x));
            } 
            else
            {
                results = from guid in results select AssetDatabase.GUIDToAssetPath(guid);
            }

            return results;
        }

        public static IEnumerable<string> FindAssetsOfType(string type)
        {
            var search = $"t:{type}";
            var results = AssetDatabase.FindAssets(search);
            return from guid in results select AssetDatabase.GUIDToAssetPath(guid);
        }


        /// <summary>
        /// Iterates all children of a game object
        /// </summary>
        /// <param name="gameObject">A root game object</param>
        /// <param name="childHandler">A function to execute on each child</param>
        /// <param name="recursive">Do it on children? (in depth)</param>
        public static void IterateChildren(GameObject gameObject, Action<GameObject> childHandler, bool recursive)
        {
            DoIterate(gameObject, childHandler, recursive);
        }

        /// <summary>
        /// NOTE: Recursive!!!
        /// </summary>
        /// <param name="gameObject">Game object to iterate</param>
        /// <param name="childHandler">A handler function on node</param>
        /// <param name="recursive">Do it on children?</param>
        private static void DoIterate(GameObject gameObject, Action<GameObject> childHandler, bool recursive)
        {
            foreach (Transform child in gameObject.transform)
            {
                childHandler(child.gameObject);
                if (recursive)
                    DoIterate(child.gameObject, childHandler, true);
            }
        }
    }
#endif
}
