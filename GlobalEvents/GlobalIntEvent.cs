﻿using UnityEngine;

namespace SMDev.Unitilities
{
    [CreateAssetMenu]
    public class GlobalIntEvent : GlobalEvent<int> { }


}

