﻿using UnityEngine;

namespace SMDev.Unitilities
{
    [CreateAssetMenu]
    public class GlobalFloatEvent : GlobalEvent<float> { }


}

