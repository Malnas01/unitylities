﻿using UnityEngine;

namespace SMDev.Unitilities
{
    public class GlobalEventTriggerBase : MonoBehaviour
    {
        public bool TriggerOnEnable = false;
        public GlobalEvent GlobalEvent;

        private void OnEnable()
        {
            if(TriggerOnEnable)
            {
                Trigger();
            }
        }

        [ContextMenu("Trigger")]
        public void Trigger()
        {
            GlobalEvent.Trigger();
        }
    }

    public abstract class GlobalEventTrigger<T> : GlobalEventTriggerBase<T>
    {
        public T Value;

        [ContextMenu("Trigger")]
        public override void Trigger()
        {
            Trigger(Value);
        }
    }

    public abstract class GlobalEventTriggerBase<T> : MonoBehaviour
    {

        public GlobalEvent<T> GlobalEvent;

        [ContextMenu("Trigger")]
        public abstract void Trigger();
        public virtual void Trigger(T argument)
        {
            GlobalEvent.Trigger(argument);  
        }
    }
}

