﻿using UnityEngine;
using UnityEngine.Events;

namespace SMDev.Unitilities
{

    public class GlobalEventListener : MonoBehaviour
    {
        [SerializeField]
        GlobalEvent GlobalEvent;

        public UnityEvent OnTrigger;

        private void OnEnable()
        {
            if(GlobalEvent != null)
            {
                GlobalEvent.Listeners.Add(this);
            }
        }

        private void OnDisable()
        {
            if (GlobalEvent != null)
            {
                GlobalEvent.Listeners.Remove(this);
            }
        }
    }
    public abstract class GlobalEventListener<T> : MonoBehaviour
    {
        [SerializeField]
        GlobalEvent<T> GlobalEvent;

        public UnityEvent<T> OnTrigger;

        private void OnEnable()
        {
            if (GlobalEvent != null)
            {
                GlobalEvent.Listeners.Add(this);
            }
        }

        private void OnDisable()
        {
            if (GlobalEvent != null)
            {
                GlobalEvent.Listeners.Remove(this);
            }
        }
    }
}

