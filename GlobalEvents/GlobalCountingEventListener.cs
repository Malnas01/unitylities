﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace SMDev.Unitilities
{
    public class GlobalCountingEventListener : MonoBehaviour
    {
        public GlobalCountingEvent GlobalCountingEvent;
        public CountingEvent[] Events;

        [System.Serializable]
        public class CountingEvent
        {
            public string name;
            public UnityEvent OnTriggered;
            public UnityEvent OnAlreadyTriggered;
            public Comparison Comparison;
            public int RequiredCount = 1;

            public bool TriggerOnce = false;
            [NonSerialized]
            public bool Triggered = false;
        }

        private void OnEnable()
        {
            if (GlobalCountingEvent != null)
            {
                GlobalCountingEvent.Listeners.Add(this);
                foreach (var e in Events)
                {
                    if (!(e.TriggerOnce && e.Triggered) && e.Comparison.Compare(GlobalCountingEvent.Count, e.RequiredCount))
                    {
                        e.Triggered = true;
                        e.OnAlreadyTriggered.Invoke();
                    }
                }
            }
        }
        public void SetEnabled(bool enabled)
        {
            GlobalCountingEvent.SetEnabled(enabled);
        }
        private void OnDisable()
        {
            if (GlobalCountingEvent != null)
            {
                GlobalCountingEvent.Listeners.Remove(this);
            }
        }

        public virtual void OnCountChanged(int count)
        {
            foreach(var e in Events)
            {
                if(!(e.TriggerOnce && e.Triggered) && e.Comparison.Compare(count,e.RequiredCount))
                {
                    e.Triggered = true;
                    e.OnTriggered.Invoke();
                }
            }
        }
    }
}

