﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace SMDev.Unitilities
{
    [CreateAssetMenu]
    public class GlobalEvent : ScriptableObject
    {
        [NonSerialized]
        public List<GlobalEventListener> Listeners = new List<GlobalEventListener>();

        private void OnEnable()
        {
            Listeners.Clear();
        }

        private void OnDisable()
        {
            Listeners.Clear();
        }

        [ContextMenu("Trigger", true)]
        bool IsPlaying() => Application.isPlaying;

        [ContextMenu("Trigger")]
        public void Trigger()
        {
            if(IsPlaying())
            {
                foreach(var g in Listeners)
                {
                    if(g != null && g.OnTrigger != null)
                    {
                        g.OnTrigger.Invoke();
                    }
                }
            }
        }
    }

    public abstract class GlobalEvent<T> : ScriptableObject
    {
        [NonSerialized]
        public List<GlobalEventListener<T>> Listeners = new List<GlobalEventListener<T>>();

        private void OnEnable()
        {
            Listeners.Clear();
        }

        private void OnDisable()
        {
            Listeners.Clear();
        }

        [ContextMenu("Trigger", true)]
        bool IsPlaying() => Application.isPlaying;

        [ContextMenu("Trigger")]
        public void Trigger(T argument)
        {
            if (IsPlaying())
            {
                foreach (var g in Listeners)
                {
                    if (g != null && g.OnTrigger != null)
                    {
                        g.OnTrigger.Invoke(argument);
                    }
                }
            }
        }
    }

#if UNITY_EDITOR
    [UnityEditor.CustomEditor(typeof(GlobalEvent))]
    public class GlobalEventEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            GUI.enabled = false;
            GlobalEvent globalEvent = target as GlobalEvent;
            if (globalEvent == null) return;

            int count = globalEvent.Listeners.Count;

            UnityEditor.EditorGUILayout.LabelField("Listeners: " + count);
            for(int i = 0; i < count; i++)
            {
                var listener = globalEvent.Listeners[i];;
                UnityEditor.EditorGUILayout.ObjectField(listener, typeof(GlobalEventListener),true);
            }
        }
    }
    [UnityEditor.CustomEditor(typeof(GlobalEvent<>))]
    public class GlobalEventTEditor : GlobalEventEditor { }
#endif
}

