﻿using UnityEngine;

namespace SMDev.Unitilities
{
    [CreateAssetMenu]
    public class GlobalGameObjectEvent : GlobalEvent<GameObject> { }


}

