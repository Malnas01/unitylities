﻿using UnityEngine;

namespace SMDev.Unitilities
{
    public class GlobalCountingEventTrigger : MonoBehaviour
    {
        public GlobalCountingEvent CountingEvent;
        public void AddCount(int count)
        {
            CountingEvent.Add(count);
        }

        public void SetCount(int count)
        {
            CountingEvent.Set(count);
        }

        public void SetEnabled(bool enabled)
        {
            CountingEvent.SetEnabled(enabled);
        }
    }
}

