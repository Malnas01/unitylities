﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace SMDev.Unitilities
{
    [CreateAssetMenu]
    public class GlobalCountingEvent : ScriptableObject
    {
        [ReadOnlyInspector]
        public string Guid;

        string EnabledKey => Guid + "_Enabled";

        [SerializeField]
        //[DisplayProperty(nameof(Enabled), drawAfter: true, MaxLines = 1)]
        bool initialEnabled = true;

        [SerializeField]
        private bool currentlyEnabled;

        bool Enabled { get => currentlyEnabled; set => currentlyEnabled = value; }

        [SerializeField, FormerlySerializedAs("count")]
        //[DisplayProperty(nameof(Count), drawAfter: true, MaxLines = 1)]
        private int initialCount = 0;

        [NonSerialized]
        public List<GlobalCountingEventListener> Listeners = new List<GlobalCountingEventListener>();

        [SerializeField]
        int currentCount;

        public int Count
        {
            get => currentCount;
            private set
            {
                currentCount = value;
                NotifyListeners();
            }
        }

        public void ResetToInitial()
        {
            Count = initialCount;
            Enabled = initialEnabled;
            if (SaveInPlayerPrefs)
            {
                PlayerPrefs.DeleteKey(Guid);
                PlayerPrefs.DeleteKey(EnabledKey);
                PlayerPrefs.Save();
            }
        }

#if UNITY_EDITOR
        [UnityEditor.MenuItem("Global Counting Events/Reset All")]
        static void ResetAllSavedValues()
        {
            var paths = EditorUtils.FindAssetsOfType(typeof(GlobalCountingEvent));
            foreach (var path in paths)
            {
                var countingEvent = UnityEditor.AssetDatabase.LoadAssetAtPath<GlobalCountingEvent>(path);
                countingEvent.ResetToInitial();
                EditorHelper.SetDirty(countingEvent);
            }

            EditorHelper.SaveAssets();
        }
#endif
        private void OnValidate()
        {
#if UNITY_EDITOR
            Guid = UnityEditor.GlobalObjectId.GetGlobalObjectIdSlow(this).ToString();
#endif
        }

        [Button(nameof(ResetToInitial), DrawAfter = true, DrawProperty = true)]
        public bool SaveInPlayerPrefs = false;

        public void OnEnable()
        {
//#if UNITY_EDITOR
//            UnityEditor.EditorApplication.playModeStateChanged += PlayModeStateChange;
//#endif
            if (SaveInPlayerPrefs && PlayerPrefs.HasKey(Guid))
            {
                Count = PlayerPrefs.GetInt(Guid, initialCount);
            }
            else
            {
                Count = initialCount;
            }

            if (SaveInPlayerPrefs && PlayerPrefs.HasKey(EnabledKey))
            {
                Enabled = PlayerPrefs.GetInt(EnabledKey, initialEnabled ? 1 : 0) != 0;
            } 
            else
            {
                Enabled = initialEnabled;
            }
        }

//        public void OnDisable()
//        {
//#if UNITY_EDITOR
//            UnityEditor.EditorApplication.playModeStateChanged -= PlayModeStateChange;
//#endif
//        }


//#if UNITY_EDITOR
//        private void PlayModeStateChange(PlayModeStateChange change)
//        {
//            if(change == UnityEditor.PlayModeStateChange.EnteredPlayMode)
//            {
//                ResetToInitial();
//            }
//        }
//#endif
        void NotifyListeners()
        {
            if (Application.isPlaying)
            {
                foreach (var g in Listeners)
                {
                    if (g != null)
                    {
                        g.OnCountChanged(Count);
                    }
                }
            }
        }

        public void SetEnabled(bool enabled)
        {
            Enabled = enabled;

            if (SaveInPlayerPrefs)
            {
                PlayerPrefs.SetInt(Guid + "_Enabled", enabled ? 1 : 0);
                PlayerPrefs.Save();
            }
        }

        public void Set(int amount)
        {
            if(!Enabled)
            {
                return;
            }
            Count = amount;
            if (SaveInPlayerPrefs)
            {
                PlayerPrefs.SetInt(Guid, Count);
                PlayerPrefs.Save();
            }
        }

        public int Add(int amount)
        {
            if (!Enabled)
            {
                return Count;
            }
            Count += amount;
            if(SaveInPlayerPrefs)
            {
                PlayerPrefs.SetInt(Guid, Count);
                PlayerPrefs.Save();
            }
            return Count;
        }
    }
}

