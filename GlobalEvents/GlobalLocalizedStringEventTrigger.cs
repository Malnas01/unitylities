﻿#if LOCALIZATION
using UnityEngine.Localization;

namespace SMDev.Unitilities
{
    public class GlobalLocalizedStringEventTrigger : GlobalEventTriggerBase<string>
    {
        public LocalizedString LocalizedString;

        public override void Trigger()
        {
            GlobalEvent.Trigger(LocalizedString.GetLocalizedString());
        }
    }
}

#endif