﻿using UnityEngine;

namespace SMDev.Unitilities
{
    [CreateAssetMenu]
    public class GlobalTransformEvent : GlobalEvent<Transform> { }


}

