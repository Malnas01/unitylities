﻿using UnityEngine;

namespace SMDev.Unitilities
{
    [CreateAssetMenu]
    public class GlobalStringEvent : GlobalEvent<string> { }


}

