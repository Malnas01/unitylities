﻿using UnityEngine;

namespace SMDev.Unitilities
{
    [CreateAssetMenu(menuName = "Animation Definition/Animation Player Scriptable Object")]
    public class AnimationPlayerScriptableObject : ScriptableObject
    {
        [SerializeReference, ChooseableSubtype]
        public AnimationPlayer Settings;
    }

}