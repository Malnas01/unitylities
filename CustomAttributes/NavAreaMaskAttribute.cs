﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SMDev.Unitilities
{
    public class NavAreaMaskAttribute : PropertyAttribute { }

#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(NavAreaMaskAttribute), true)]
    public class NavAreaMaskDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty serializedProperty, GUIContent label)
        {

            using (new GUILayout.HorizontalScope())
            {
                EditorGUI.BeginChangeCheck();
#if UNITY_6000_0_OR_NEWER
                string[] areaNames = UnityEngine.AI.NavMesh.GetAreaNames();
#else
                string[] areaNames = GameObjectUtility.GetNavMeshAreaNames();
#endif
                int mask = serializedProperty.intValue;
                mask = EditorGUI.MaskField(position, label, mask, areaNames);
                if (EditorGUI.EndChangeCheck())
                {
                    serializedProperty.intValue = mask;
                }
            }
        }
    }
#endif
}
