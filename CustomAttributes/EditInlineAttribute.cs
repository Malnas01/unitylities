using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SMDev.Unitilities
{
    public class EditInlineAttribute : PropertyAttribute { }

#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(EditInlineAttribute), true)]
    public class EditInlineObjectDrawer : PropertyDrawer
    {

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            var height = EditorGUIUtility.singleLineHeight;
            if (property.isExpanded && property.objectReferenceValue != null)
            {
                var obj = new SerializedObject(property.objectReferenceValue);

                obj.UpdateIfRequiredOrScript();
                SerializedProperty iterator = obj.GetIterator();
                bool enterChildren = true;
                while (iterator.NextVisible(enterChildren))
                {
                    height += EditorGUI.GetPropertyHeight(iterator, true);
                    height += EditorGUIUtility.standardVerticalSpacing;
                    enterChildren = false;
                }
            }
            return height;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var guiContent = new GUIContent(property.displayName);
            var foldoutRect = new Rect(position.x, position.y, EditorGUIUtility.labelWidth, EditorGUIUtility.singleLineHeight);
            property.isExpanded = EditorGUI.Foldout(foldoutRect, property.isExpanded, guiContent, true);

            var indentedPosition = EditorGUI.IndentedRect(position);
            var indentOffset = indentedPosition.x - position.x;
            var propertyRect = new Rect(position.x + (EditorGUIUtility.labelWidth - indentOffset), position.y, position.width - (EditorGUIUtility.labelWidth - indentOffset), EditorGUIUtility.singleLineHeight);

            EditorGUI.PropertyField(propertyRect, property, GUIContent.none, true);

            if (property.isExpanded && property.objectReferenceValue != null)
            {
                EditorGUI.indentLevel++;

                var obj = new SerializedObject(property.objectReferenceValue);

                obj.UpdateIfRequiredOrScript();
                SerializedProperty iterator = obj.GetIterator();
                bool enterChildren = true;
                Rect rect = new Rect(position.x, propertyRect.y, position.width, propertyRect.height);
                while (iterator.NextVisible(enterChildren))
                {
                    rect.y += rect.height;
                    rect.y += EditorGUIUtility.standardVerticalSpacing;
                    rect.height = EditorGUI.GetPropertyHeight(iterator);
                    using (new EditorGUI.DisabledScope("m_Script" == iterator.propertyPath))
                    {
                        EditorGUI.PropertyField(rect, iterator, true);
                    }

                    enterChildren = false;
                }

                obj.ApplyModifiedProperties();
                obj.Dispose();

                EditorGUI.indentLevel--;
            }
        }
    }
#endif
}
