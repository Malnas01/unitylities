﻿namespace SMDev.Unitilities
{
    public enum Comparison
    {
        Equal,
        GreaterThan,
        GreaterThanOrEqual,
        LessThan,
        LessThanOrEqual,
        NotEqual,
    }

    public static class ComparisonExtension
    {
        public static bool Compare(this Comparison comparison, int leftHand, int rightHand)
        {
            return comparison switch
            {
                Comparison.GreaterThan => leftHand > rightHand,
                Comparison.GreaterThanOrEqual => leftHand >= rightHand,
                Comparison.LessThan => leftHand < rightHand,
                Comparison.LessThanOrEqual => leftHand <= rightHand,
                Comparison.NotEqual => leftHand != rightHand,
                _ => leftHand == rightHand,
            };
        }

        public static bool Compare(this Comparison comparison, float leftHand, float rightHand)
        {
            return comparison switch
            {
                Comparison.GreaterThan => leftHand > rightHand,
                Comparison.GreaterThanOrEqual => leftHand >= rightHand,
                Comparison.LessThan => leftHand < rightHand,
                Comparison.LessThanOrEqual => leftHand <= rightHand,
                Comparison.NotEqual => leftHand != rightHand,
                _ => leftHand == rightHand,
            };
        }
        public static bool Compare(this Comparison comparison, double leftHand, double rightHand)
        {
            return comparison switch
            {
                Comparison.GreaterThan => leftHand > rightHand,
                Comparison.GreaterThanOrEqual => leftHand >= rightHand,
                Comparison.LessThan => leftHand < rightHand,
                Comparison.LessThanOrEqual => leftHand <= rightHand,
                Comparison.NotEqual => leftHand != rightHand,
                _ => leftHand == rightHand,
            };
        }

        public static bool Compare(this Comparison comparison, bool leftHand, bool rightHand)
        {
            return comparison switch
            {
                Comparison.GreaterThan => leftHand && !rightHand,
                Comparison.GreaterThanOrEqual => (leftHand && !rightHand) || leftHand == rightHand,
                Comparison.LessThan => !leftHand && rightHand,
                Comparison.LessThanOrEqual => (!leftHand && rightHand) || leftHand == rightHand,
                Comparison.NotEqual => leftHand != rightHand,
                _ => leftHand == rightHand,
            };
        }
    }
}
