﻿using System;

namespace SMDev.Unitilities
{
    [AttributeUsage(AttributeTargets.Method)]
    public class ContextualMenuItemAttribute : Attribute
    {
        public string itemName { get; set; }
        public Type validatorType { get; set; }

        public string additionalValidator { get; set; }
        public ContextualMenuItemAttribute(string itemName, Type validatorType = null, string additionalValidator = null)
        {
            this.itemName = itemName;
            this.validatorType = validatorType;
            this.additionalValidator = additionalValidator;
        }
    }
}
