﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using UnityEngine;


#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SMDev.Unitilities
{
    public class ChoosableElementSettingsAttribute : Attribute
    {
        public readonly ChooseableSubtypeAttribute Settings;

        public ChoosableElementSettingsAttribute(bool allowNull = false, bool showLabel = true, bool showFoldout = true, Type typeOverride = null, Type[] excludeTypes = null)
        {
            Settings = new ChooseableSubtypeAttribute(allowNull, showLabel, showFoldout, typeOverride, excludeTypes);
        }

    }

    public class ExcludeFromChooseableSubtypeAttribute : Attribute
    {

    }
    public class ChooseableSubtypeAttribute : PropertyAttribute
    {
        public bool ShowFoldout { get; set; }
        public bool ShowLabel { get; set; }
        public bool AllowNull { get; set; }
        public Type Type { get; set; }

        public readonly Type[] ExcludeTypes;
        public ChooseableSubtypeAttribute(bool allowNull = false, bool showLabel = true, bool showFoldout = true, Type typeOverride = null, Type[] excludeTypes = null)
        {
            ShowLabel = showLabel;
            Type = typeOverride;
            AllowNull = allowNull;
            ShowFoldout = showFoldout;
            ExcludeTypes = excludeTypes;
        }
    }

    [System.Serializable]
    public class ChoosableElementBase<T> where T : class
    {
        [SerializeReference]
        public T Element;

        public static implicit operator T(ChoosableElementBase<T> element)
        {
            return element?.Element;
        }
        public static implicit operator ChoosableElementBase<T>(T element)
        {
            return new ChoosableElementBase<T> { Element = element };
        }
    }

    [ChoosableElementSettings]
    [System.Serializable]
    public class ChoosableElement<T> : ChoosableElementBase<T> where T : class { }


    [ChoosableElementSettings(true)]
    [System.Serializable]
    public class NullableChoosableElement<T> : ChoosableElementBase<T>  where T : class{ }


#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(ChoosableElementBase<>), true)]
    public class ChooseableElementDrawer : GenericChildDrawer
    {
        protected override ChooseableSubtypeAttribute GetChooseableSubtypeAttribute(SerializedProperty property)
        {
            var x = fieldInfo.FieldType.GetCustomAttribute<ChoosableElementSettingsAttribute>();
            return x?.Settings ?? new ChooseableSubtypeAttribute();
        }

        protected override bool HasSerializeReference => true;

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            property = property.FindPropertyRelative("Element");
            label.text = property.displayName;
            return base.GetPropertyHeight(property, label);
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            property = property.FindPropertyRelative("Element");
            label.text = property.displayName;
            base.OnGUI(position, property, label);
        }
    }


    [CustomPropertyDrawer(typeof(ChooseableSubtypeAttribute))]
    public class GenericChildDrawer : PropertyDrawer
    {
        protected virtual object CreateNew(Type t) => t.GetConstructor(Type.EmptyTypes)?.Invoke(null) ?? RuntimeHelpers.GetUninitializedObject(t);

        protected virtual bool HasSerializeReference => fieldInfo.GetCustomAttribute<SerializeReference>() != null;

        protected virtual ChooseableSubtypeAttribute GetChooseableSubtypeAttribute(SerializedProperty property) => attribute as ChooseableSubtypeAttribute;

        public GenericChildDrawer()
        {
        }

        Type baseType;
        List<Type> childTypesCache;
        List<Type> GetValidTypes(Type baseType, bool allowNull = false, IEnumerable<Type> excludeTypes = null)
        {
            if (this.baseType != baseType)
            {
                var types = EditorUtils.GetSubclasses(baseType).Where(t=>t.IsAbstract == false);

                if (baseType.IsAbstract == false)
                {
                    types = types.Prepend(baseType);
                }

                types = types.Where(x => x?.GetCustomAttribute<ExcludeFromChooseableSubtypeAttribute>() == null);

                if(excludeTypes != null)
                {
                    types = types.Where(x => !excludeTypes.Contains(x));
                }

                if (allowNull)
                {
                    types = types.Prepend(null);
                }

                childTypesCache = types.ToList();
                this.baseType = baseType;
            }

            return childTypesCache;
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            var height = EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;

            if (!HasSerializeReference)
            {
                return height;
            }

            if(property.isExpanded == false)
            {
                return height;
            }

            //var subtypeAtt = attribute as ChooseableSubtypeAttribute;
            var fieldType = property.managedReferenceValue?.GetType();// ?? fieldInfo?.FieldType;
            
            if (fieldType != null && fieldType.IsAbstract == false)
            {
                var iterator = property.GetEnumerator();
                int depth = property.depth + 1;
                while (iterator.MoveNext())
                {
                    var prop = (SerializedProperty)iterator.Current;
                    if (prop.depth > depth) continue;
                    height += EditorGUI.GetPropertyHeight(iterator.Current as SerializedProperty) + EditorGUIUtility.standardVerticalSpacing;
                }
            }
            return height;
        }
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (!HasSerializeReference)
            {
                EditorGUI.LabelField(position, "<color=red>Generic Child Drawer requires field to be marked as [SerializeReference]</color>", new GUIStyle() { richText = true });
                return;
            }

            var subtypeAtt = GetChooseableSubtypeAttribute(property);//  attribute as ChooseableSubtypeAttribute;

            Type underlying = subtypeAtt?.Type;

            if(underlying == null)
            {
                var typename = property.managedReferenceFieldTypename.Split(" ");

                var assembly = EditorUtils.GetLoadedAssemblyFromName(typename[0]);
                if(assembly != null)
                {
                    underlying = assembly.GetType(typename[1]);
                }

                if(underlying == null && fieldInfo != null)
                {
                    if(fieldInfo.FieldType.IsArray)
                    {
                        underlying = fieldInfo.FieldType.GetElementType();
                    } 
                    else if(fieldInfo.FieldType.IsConstructedGenericType && fieldInfo.FieldType.GetGenericTypeDefinition() == typeof(List<>))
                    {
                        underlying = fieldInfo.FieldType.GetGenericArguments()[0];
                    }

                    if(underlying == null)
                    {
                        underlying = property.managedReferenceValue?.GetType();
                    }
                }
            }

            //Assembly.
            //var underlying = subtypeAtt?.Type ?? fieldInfo.ReflectedType.Assembly.GetType(property.managedReferenceFieldTypename.Split(" ")[1]) ?? fieldInfo?.FieldType ?? property.managedReferenceValue?.GetType();

            var childTypes = GetValidTypes(underlying, subtypeAtt.AllowNull, subtypeAtt.ExcludeTypes);


            position.height = EditorGUIUtility.singleLineHeight;
            var fieldType = property.managedReferenceValue?.GetType();
            var names = childTypes.Select(x => x != null ? ObjectNames.NicifyVariableName(x.Name) : "None").ToArray();

            var text = subtypeAtt.ShowLabel ? label.text : "";
            var labelRect = new Rect(position.x, position.y, EditorGUIUtility.labelWidth, EditorGUIUtility.singleLineHeight);

            if(subtypeAtt.ShowFoldout)
            {
                property.isExpanded = EditorGUI.Foldout(labelRect, property.isExpanded, text, !string.IsNullOrEmpty(text));  //If toggle on label click is true when we have no label, it will toggle when interacting with the dropdown so we need this false in that case
                if (subtypeAtt.ShowLabel == false)
                {
                    position.xMin += EditorStyles.foldout.padding.left;
                }
            }
            else
            {
                property.isExpanded = true;
            }

            var choice = EditorGUI.Popup(position, subtypeAtt.ShowLabel ? (subtypeAtt.ShowFoldout?" " : text) : "", childTypes.IndexOf(fieldType), names);
            if (choice < 0)
            {
                choice = 0;
            }

            if ((fieldType == null && !subtypeAtt.AllowNull) || (fieldType != null && !underlying.IsAssignableFrom(fieldType)) || childTypes[choice] != fieldType)
            {
                property.managedReferenceValue = childTypes[choice] == null ? null : CreateNew(childTypes[choice]);
                fieldType = childTypes[choice];
                property.serializedObject.ApplyModifiedProperties();
                //underlying = childTypes[choice];
            }

            if(property.isExpanded)
            {
                if (fieldType != null && fieldType.IsAbstract == false)
                {
                    EditorGUI.indentLevel++;
                    position.y += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
                    int depth = property.depth + 1;
                    var iterator = property.GetEnumerator();
                    while (iterator.MoveNext())
                    {
                        var prop = (SerializedProperty)iterator.Current;
                        if (prop.depth > depth) continue;
                        position.height = EditorGUI.GetPropertyHeight(prop);
                        EditorGUI.PropertyField(position, prop, true);
                        position.y += position.height + EditorGUIUtility.standardVerticalSpacing;
                    }

                    EditorGUI.indentLevel--;
                }
            }
        }
    }
#endif
}
