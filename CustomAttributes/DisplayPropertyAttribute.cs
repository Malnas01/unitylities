﻿using UnityEngine;
using System.Reflection;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SMDev.Unitilities
{
    public class DisplayPropertyAttribute : PropertyAttribute
    {
        public string PropertyName;

        public bool DrawField = true;
        public bool DrawAfter = false;
        public string EvaluationFailedMessage = null;
        public int MaxLines = 1;
        public DisplayPropertyAttribute(string propertyName, bool drawField = true, bool drawAfter = false, string evaluationFailedMessage = null, bool useTextArea = false, int maxLines = 0)
        {
            PropertyName = propertyName;
            DrawField = drawField;
            DrawAfter = drawAfter;
            EvaluationFailedMessage = evaluationFailedMessage;
            MaxLines = maxLines;
        }
    }


#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(DisplayPropertyAttribute))]
    public class DisplayPropertyDrawer : PropertyDrawer
    {
        Vector2 scrollPosition = Vector2.zero;
        float lastWidth = 500f;
        float GetTextBoxHeight(SerializedProperty property)
        {
            var att = attribute as DisplayPropertyAttribute;
            if (att.MaxLines <= 0)
            {
                var content = new GUIContent(GetStringValue(property));
                var size = EditorStyles.textArea.CalcSize(content);

                var boxWidth = lastWidth - EditorGUIUtility.labelWidth - 2f;

                if(size.x > boxWidth)
                {
                    size.y += GUI.skin.horizontalScrollbar.fixedHeight;
                }

                return Mathf.Max(size.y, EditorGUIUtility.singleLineHeight);
            } 
            else
            {
                return EditorGUIUtility.singleLineHeight * att.MaxLines;
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            var att = attribute as DisplayPropertyAttribute;
            var height = GetTextBoxHeight(property);

            if (att.DrawField)
            {
                height += EditorGUI.GetPropertyHeight(property, true) + EditorGUIUtility.standardVerticalSpacing;
            }
            return height;
        }

        string GetStringValue(SerializedProperty property)
        {
            var att = attribute as DisplayPropertyAttribute;
            try
            {
                var target = property.GetParentObject(out var type);
                var p = fieldInfo.DeclaringType.GetProperty(att.PropertyName, EditorUtils.AllBindingFlags);

                return p.GetValue(target).ToString();
            }
            catch (TargetInvocationException e)
            {
                return string.IsNullOrEmpty(att.EvaluationFailedMessage) ? $"Evaluation Error: {e.InnerException}" : att.EvaluationFailedMessage;
            }
            catch (Exception e)
            {
                return $"Error: {e}";
            }
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var att = attribute as DisplayPropertyAttribute;

            //var rect = EditorGUILayout.GetControlRect(true, 0);
            if (Event.current.type == EventType.Repaint)
            {
                //lastWidth = rect.width - rect.x;
                lastWidth = position.width - position.x;
            }

            var textBoxRect = new Rect(position);
            textBoxRect.height = GetTextBoxHeight(property);

            var propertyRect = new Rect(position);
            propertyRect.height = EditorGUI.GetPropertyHeight(property, true);

            if (att.DrawField && att.DrawAfter)
            {
                EditorGUI.PropertyField(propertyRect, property, new GUIContent() { text = property.displayName }, true);
                textBoxRect.y = propertyRect.y + propertyRect.height + EditorGUIUtility.standardVerticalSpacing;
            }
           
            var valueText = GetStringValue(property);

            textBoxRect = EditorGUI.PrefixLabel(textBoxRect, new GUIContent(ObjectNames.NicifyVariableName(att.PropertyName)));

            var contentSize = EditorStyles.textArea.CalcSize(new GUIContent(valueText));

            var verticalScroll = contentSize.y > textBoxRect.height;
            var horizontalScroll = contentSize.x > textBoxRect.width;

            var useScrollView = att.MaxLines != 1 && (verticalScroll || horizontalScroll);

            var scrollViewRect = textBoxRect;
            if (useScrollView)
            {

                textBoxRect.width = Mathf.Max(contentSize.x, textBoxRect.width - GUI.skin.verticalScrollbar.fixedWidth); 
                textBoxRect.height = Mathf.Max(contentSize.y, textBoxRect.height - GUI.skin.horizontalScrollbar.fixedHeight);
                
                scrollPosition = GUI.BeginScrollView(scrollViewRect, scrollPosition, textBoxRect);
            }

            using (new EditorGUI.DisabledScope(true))
            {
                //Indend affects PrefixLabel and since we already drew it manually earlier,
                //when the control attempts to draw an empty prefix label it will apply the indent again, so we 0 it out to prevent that
                var indent = EditorGUI.indentLevel;
                EditorGUI.indentLevel = 0;
                EditorGUI.TextArea(textBoxRect, valueText);
                EditorGUI.indentLevel = indent;
            }

            if(useScrollView)
            {
                GUI.EndScrollView();
            }


            if (att.DrawField && att.DrawAfter == false)
            {
                propertyRect.y = scrollViewRect.y + scrollViewRect.height + EditorGUIUtility.standardVerticalSpacing;
                EditorGUI.PropertyField(propertyRect, property, new GUIContent() { text = property.displayName }, true);
            }
        }

    }
#endif
}