﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SMDev.Unitilities
{
    public class ValidateAttribute : PropertyAttribute 
    {
        public string validateMethod;
        public string fixMethod;
        public string validateTooltip;
        public ValidateAttribute(string validateMethod, string fixMethod = "", string validateTooltip = null)
        {
            this.validateMethod = validateMethod;
            this.fixMethod = fixMethod;
            this.validateTooltip = validateTooltip;
        }
    }

#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(ValidateAttribute))]
    class FieldValidator : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var validateAttribute = attribute as ValidateAttribute;
            var type = property.serializedObject.targetObject.GetType();
            var validateMethod = type.GetMethod(validateAttribute.validateMethod, EditorUtils.AllBindingFlags);
            var fixMethod = type.GetMethod(validateAttribute.fixMethod, EditorUtils.AllBindingFlags);


            EditorGUI.BeginProperty(position, label, property);
            const float buttonWidth = 20f;
            const float padding = 2f;

            GameObject go = property.objectReferenceValue as GameObject;
            if (property.objectReferenceValue is Component comp)
            {
                go = comp.gameObject;
            }

            if (go != null)
            {
                bool isValid = (bool)validateMethod.Invoke(property.serializedObject.targetObject,new object[] { property.objectReferenceValue });

                Color prevBackgroundColor = GUI.backgroundColor;
                var buttonStyle = new GUIStyle(EditorStyles.miniButtonRight);
                buttonStyle.fontStyle = FontStyle.Bold;

                GUI.backgroundColor = Color.green;

                Rect buildSettingsPos = position;
                buildSettingsPos.x += position.width - buttonWidth + padding;
                buildSettingsPos.width = buttonWidth;

                GUI.enabled = false;
                GUIContent settingsContent = new GUIContent("✔️", "Object is Valid");

                if (!isValid)
                {
                    GUI.enabled = fixMethod != null ;
                    settingsContent = new GUIContent("!", validateAttribute.validateTooltip == null ? "Object is Invalid" : validateAttribute.validateTooltip);
                    GUI.backgroundColor = Color.red;
                }

                if (GUI.Button(buildSettingsPos, settingsContent, buttonStyle) && fixMethod != null)
                {
                    fixMethod.Invoke(property.serializedObject.targetObject, new object[] { property.objectReferenceValue });
                }

                GUI.enabled = true;
                GUI.backgroundColor = prevBackgroundColor;
                position.width -= buttonWidth;
            }

            //var t = EditorGUI.ObjectField(position, label, property.objectReferenceValue, property.GetTypeFromSerializedProperty(), true);
            //if (property.objectReferenceValue != t)
            //{
            //    property.objectReferenceValue = t;
            //}

            EditorGUI.PropertyField(position, property);

            EditorGUI.EndProperty();
        }
    }
#endif
}
