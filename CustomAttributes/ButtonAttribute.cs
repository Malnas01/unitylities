﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SMDev.Unitilities
{
    public class ButtonAttribute : PropertyAttribute
    {
        public string MethodName;
        public string ButtonText;
        public bool DrawProperty = true;
        public bool DrawAfter = true;
        public ButtonAttribute(string methodName, string buttonText = null)
        {
            MethodName = methodName;
            ButtonText = buttonText;
        }
    }


#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(ButtonAttribute))]
    public class ButtonDrawer : PropertyDrawer
    {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            var att = attribute as ButtonAttribute;
            if (att.DrawProperty)
            {
                return EditorGUIUtility.singleLineHeight + EditorGUI.GetPropertyHeight(property) + EditorGUIUtility.standardVerticalSpacing;
            }
            return EditorGUIUtility.singleLineHeight;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var att = attribute as ButtonAttribute;
            var target = EditorUtils.GetParentObject(property, out var type);

            //var type = target.GetType();
            var p = type.GetMethod(att.MethodName, EditorUtils.AllBindingFlags);

            if (att.DrawProperty && att.DrawAfter)
            {
                position.height = EditorGUI.GetPropertyHeight(property);
                EditorGUI.PropertyField(position, property, label);
                position.y += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
            }

            if (p != null)
            {
                var wasEnabled = GUI.enabled;
                GUI.enabled = true;
                position.height = EditorGUIUtility.singleLineHeight;
                if(GUI.Button(position, string.IsNullOrEmpty(att.ButtonText) ? ObjectNames.NicifyVariableName(att.MethodName) : att.ButtonText))
                {
                    p.Invoke(target, new object[] {});
                }
                
                GUI.enabled = wasEnabled;
            }
            if (att.DrawProperty && att.DrawAfter == false)
            {
                position.y += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
                position.height = EditorGUI.GetPropertyHeight(property);
                EditorGUI.PropertyField(position, property, label);
            }
        }

    }
#endif
}