﻿using UnityEngine;
using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SMDev.Unitilities
{
    public class ShowIfAttribute : PropertyAttribute
    {
        internal string Property;
        internal double Value;
        internal Comparison comparison;
        public bool SetReadonly { get; set; }
        public ShowIfAttribute(string property, double value, Comparison comparison = Comparison.Equal)
        {
            Property = property;
            Value = value;
            this.comparison = comparison;
        }
        public ShowIfAttribute(string property, bool value = true, Comparison comparison = Comparison.Equal)
        {
            Property = property;
            Value = value ? 1 : 0;
            this.comparison = comparison;
        }
#if UNITY_EDITOR
        public bool Compare(SerializedProperty prop)
        {
            if (prop == null)
            {
                Debug.LogWarning("Can't find property " + Property);
                return true;
            }
            return Compare(prop.propertyType switch
            {
                SerializedPropertyType.Float => prop.floatValue,
                SerializedPropertyType.ObjectReference => prop.objectReferenceValue == null ? 0 : 1,
                SerializedPropertyType.Generic => prop.isArray ? prop.arraySize : (prop.objectReferenceValue == null ? 0 : 1),
                _ => prop.intValue
            });
        }
#endif
        public bool Compare(double value)
        {
            return comparison.Compare(value, Value);
        }
    }

#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(ShowIfAttribute))]
    public class ShowIfDrawer : PropertyDrawer
    {

        bool ShouldShow(SerializedProperty property)
        {
            var att = attribute as ShowIfAttribute;
            var prop = property.serializedObject.FindProperty(att.Property);

            if (prop != null)
            {
                return att.Compare(prop);
            }
            else
            {
                var parent = EditorUtils.GetParentObject(property, out _);
                var value = EditorUtils.GetFieldPropertyOrReturnValue(parent, att.Property);
                try
                {
                    double valueAsDouble;
                    if (value is IConvertible)
                    {
                        valueAsDouble = Convert.ToDouble(value);
                    }
                    else if(value?.Equals(null)??true)
                    {
                        valueAsDouble = 0;
                    } else
                    {
                        valueAsDouble = 1;
                    }
                    return att.Compare(valueAsDouble);
                }
                catch { }
            }
            return true;
        }
        public override float GetPropertyHeight(SerializedProperty property,
                                                GUIContent label)
        {
            var att = attribute as ShowIfAttribute;
            if (att.SetReadonly)
            {
                return EditorGUI.GetPropertyHeight(property, label, true);
            }

            if (ShouldShow(property))
            {
                return EditorGUI.GetPropertyHeight(property, label, true);
            }
            else
            {
                return 0;
            }
        }

        public override void OnGUI(Rect position,
                                   SerializedProperty property,
                                   GUIContent label)
        {
            var att = attribute as ShowIfAttribute;

            if (!ShouldShow(property))
            {
                if (att.SetReadonly)
                {
                    GUI.enabled = false;
                }
                else
                {
                    return;
                }
            }

            EditorGUI.PropertyField(position, property, label, true);
            GUI.enabled = true;
        }
    }
#endif
}
