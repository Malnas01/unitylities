﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SMDev.Unitilities
{
    public class PrefabAttribute : PropertyAttribute { }

#if UNITY_EDITOR
    [CustomPropertyDrawer(typeof(PrefabAttribute))]
    class PrefabInSceneChecker : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (property.hasMultipleDifferentValues)
            {
                GUI.enabled = false;
                EditorGUI.TextField(position, label, "Multi-Object editing not supported");
                GUI.enabled = true;
                return;
            }

            EditorGUI.BeginProperty(position, label, property);
            const float buttonWidth = 20f;
            const float padding = 2f;

            GameObject go = property.objectReferenceValue as GameObject;
            if (property.objectReferenceValue is Component comp)
            {
                go = comp.gameObject;
            }

            if (go != null)
            {
                bool isPrefabRoot = EditorUtils.IsPrefabRoot(go); // PrefabUtility.IsAnyPrefabInstanceRoot(go) || (PrefabUtility.GetPrefabAssetType(property.objectReferenceValue) != PrefabAssetType.NotAPrefab && PrefabUtility.GetPrefabInstanceStatus(property.objectReferenceValue) == PrefabInstanceStatus.NotAPrefab);

                var isSceneObject = go.scene != null && go.scene.IsValid();
                Color prevBackgroundColor = GUI.backgroundColor;
                var buttonStyle = new GUIStyle(EditorStyles.miniButtonRight);
                buttonStyle.fontStyle = FontStyle.Bold;

                GUI.backgroundColor = Color.green;

                Rect buildSettingsPos = position;
                buildSettingsPos.x += position.width - buttonWidth + padding;
                buildSettingsPos.width = buttonWidth;

                GUI.enabled = false;
                GUIContent settingsContent = new GUIContent("✔️", "Object is Prefab");

                if (!isPrefabRoot)
                {
                    settingsContent = new GUIContent("?", "Field is pointing to a scene object that is not a prefab root.");
                    GUI.backgroundColor = Color.yellow;
                }
                else if (isSceneObject)
                {
                    GUI.enabled = true;
                    settingsContent = new GUIContent("!", "Field is pointing to a scene object. Click here to point it at the prefab.");
                    GUI.backgroundColor = Color.red;
                }

                if (GUI.Button(buildSettingsPos, settingsContent, buttonStyle))
                {
                    property.objectReferenceValue = AssetDatabase.LoadAssetAtPath<GameObject>(PrefabUtility.GetPrefabAssetPathOfNearestInstanceRoot(property.objectReferenceValue));
                }

                GUI.enabled = true;
                GUI.backgroundColor = prevBackgroundColor;
                position.width -= buttonWidth;
            }

            var t = EditorGUI.ObjectField(position, label, property.objectReferenceValue, property.GetTypeFromSerializedProperty(), true);
            if (property.objectReferenceValue != t) 
            {
                property.objectReferenceValue = t;
                if(t != null && property.objectReferenceValue == null)
                {
                    property.objectReferenceValue = AssetDatabase.LoadAssetAtPath<GameObject>(PrefabUtility.GetPrefabAssetPathOfNearestInstanceRoot(t));
                }
            }

            //EditorGUI.PropertyField(position, property);

            EditorGUI.EndProperty();
        }
    }
#endif
}
