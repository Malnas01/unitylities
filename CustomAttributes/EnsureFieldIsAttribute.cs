﻿using System;
using System.Linq;
using System.Reflection;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace SMDev.Unitilities
{
    public class EnsureFieldIsAttribute : PropertyAttribute
    {
        public Type type;

        public EnsureFieldIsAttribute(Type type)
        {
            this.type = type;
        }
    }

    [Serializable]
    public class SerializableInterfaceBase 
    {
        [SerializeField]
        protected Component _component;

        public Component BaseComponent => _component;

        public override bool Equals(object obj)
        {
            return (this == obj || this._component == obj || this._component.Equals(obj) || (obj is SerializableInterfaceBase objB && this._component == objB._component));
        }
        public override int GetHashCode()
        {
            return _component?.GetHashCode()??0;
        }
        public static bool operator ==(SerializableInterfaceBase b1, SerializableInterfaceBase b2)
        {
            return b1.Equals(b2);
        }
        public static bool operator !=(SerializableInterfaceBase b1, SerializableInterfaceBase b2)
        {
            return !b1.Equals(b2);
        }
    }

    [Serializable]
    public struct ComponentWrapper<T> where T : class
    {
        [SerializeField]
        Component _component;

        public Component BaseComponent => _component;

        public override bool Equals(object obj)
        {
            return (/*this == obj || this._component == obj ||*/ this._component.Equals(obj) || (obj is ComponentWrapper<T> objB && this._component == objB._component));
        }
        public override int GetHashCode()
        {
            return _component?.GetHashCode() ?? 0;
        }
        public static bool operator ==(ComponentWrapper<T> b1, ComponentWrapper<T> b2)
        {
            return b1.Equals(b2);
        }
        public static bool operator !=(ComponentWrapper<T> b1, ComponentWrapper<T> b2)
        {
            return !b1.Equals(b2);
        }

        public static implicit operator T(ComponentWrapper<T> wrapped)
        {
            return wrapped.Component;
        }
        public static implicit operator ComponentWrapper<T>(T unwraped)
        {
            return new ComponentWrapper<T>(unwraped);
        }

        T cachedCast;
        public T Component
        {
            get 
            { 
                if(cachedCast == null)
                {
                    if (_component == null) return null;
                    cachedCast = _component as T;
                }
                return cachedCast; 
            }
            set
            {
                var type = value.GetType();
                //if(!typeof(T).IsAssignableFrom(type))
                //{
                //    Debug.LogError($"Type of value {value.GetType()} is not type {typeof(T)}");
                //    return;
                //}
                if(!typeof(Component).IsAssignableFrom(type))
                {
                    Debug.LogError($"Type of value {value.GetType()} is not of type {typeof(Component)}");
                    return;
                }
                _component = value as Component;
                cachedCast = value;
            }
        }

        public ComponentWrapper(T component)
        {
            if (!typeof(Component).IsAssignableFrom(component.GetType()))
            {
                throw new InvalidCastException($"Type of value {component.GetType()} is not of type {typeof(Component)}");
            }
            cachedCast = component;
            _component = component as Component;
        }
    }

#if UNITY_EDITOR

    [CustomPropertyDrawer(typeof(ComponentWrapper<>))]
    public class SerializableInterfaceDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var genericType = property.GetTypeFromSerializedProperty().GenericTypeArguments[0];
            property.RunPerMultipleDifferentValue((prop) =>
            {
                prop.NextVisible(true);
                EditorGUI.PropertyField(position, prop, label);
                EnsureFieldIsDrawer.EnsureProperty(prop, genericType);
            });
        }
    }

    [CustomPropertyDrawer(typeof(EnsureFieldIsAttribute))]
    public class EnsureFieldIsDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            
            EditorGUI.PropertyField(position, property, label, true);

            var requiredType = fieldInfo.GetCustomAttributes<EnsureFieldIsAttribute>(true).FirstOrDefault();
            property.RunPerMultipleDifferentValue((prop) =>
            {
                if (prop.isArray)
                {
                    for (int j = 0; j < prop.arraySize; j++)
                    {
                        EnsureProperty(prop.GetArrayElementAtIndex(j), requiredType.type);
                    }
                }
                else
                {
                    EnsureProperty(prop, requiredType.type);
                }

                prop.serializedObject.ApplyModifiedProperties();
            });
        }

        public static void EnsureProperty(SerializedProperty property, Type requiredType)
        {
            if (property.objectReferenceValue != null)
            {
                if (!requiredType.IsAssignableFrom(property.objectReferenceValue.GetType()))
                {
                    if (property.objectReferenceValue is GameObject go) property.objectReferenceValue = go?.GetComponent(requiredType);
                    else if (property.objectReferenceValue is Component component) property.objectReferenceValue = component?.GetComponent(requiredType);
                    else property.objectReferenceValue = null;

                    if (property.objectReferenceValue == null)
                    {
                        Debug.LogError($"EnsureFieldIs: {property.name} is not assignable to type {requiredType}");
                    }
                }
            }
        }
    }
#endif
}
