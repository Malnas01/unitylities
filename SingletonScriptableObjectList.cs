﻿#if UNITY_EDITOR
#endif

using System.IO;
using UnityEditor;
using UnityEngine;

namespace SMDev.Unitilities
{
    public class SingletonScriptableObjectList<SelfReferenceType,U> : ScriptableObjectList<U> where U : ScriptableObjectListItem where SelfReferenceType : SingletonScriptableObjectList<SelfReferenceType, U>
    {
        public static string path;
        static SelfReferenceType soInstance;

        public static SelfReferenceType Singleton
        {
            get
            {
                if(soInstance == null)
                {
                    soInstance = Resources.Load<SelfReferenceType>(typeof(SelfReferenceType).Name);
                }
                return soInstance;
            }
        }

        public static U FromId(int id)
        {
            return Singleton.Items[id];
        }
    }
    public abstract class ScriptableObjectSingelton<T> : ScriptableObject where T : ScriptableObjectSingelton<T>
    {
        static T soInstance;
        public static T Singleton
        {
            get
            {
                if (soInstance == null)
                {
                    soInstance = Resources.Load<T>(typeof(T).Name);
                }
                return soInstance;
            }
        }
    }

#if UNITY_EDITOR

    [CustomEditor(typeof(SingletonScriptableObjectList<,>), true)]
    public class SingletonScriptableObjectListEditor : ScriptableObjectListEditor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            ScriptableObjectSingelton.DrawEnsureResourcesFolderUI(this);
        }
    }

    [CustomEditor(typeof(ScriptableObjectSingelton<>), true)]
    public class ScriptableObjectSingelton : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            DrawEnsureResourcesFolderUI(this);
        }

        public static void DrawEnsureResourcesFolderUI(Editor editor)
        {
            const string resourcesString = "Resources";
            var path = AssetDatabase.GetAssetPath(editor.target);
            var directory = string.IsNullOrEmpty(path) ? "" : Path.GetDirectoryName(path);
            var typeName = editor.serializedObject.targetObject.GetType().Name;

            var inResources = Path.GetFileName(directory).Contains(resourcesString);
            var correctName = Path.GetFileNameWithoutExtension(path) == typeName;
            var oldColor = GUI.backgroundColor;

            if (!inResources || !correctName) GUI.backgroundColor = Color.red;

            GUI.enabled = false;
            GUILayout.TextField(path);

            if (!inResources || !correctName) GUI.enabled = true;
            if (GUILayout.Button("Move to Resources Folder"))
            {
                string newFolder = directory;
                if (!inResources)
                {
                    newFolder = Path.Combine(directory, resourcesString);
                    if (!AssetDatabase.IsValidFolder(newFolder)) AssetDatabase.CreateFolder(directory, resourcesString);
                }

                var newPath = Path.Combine(newFolder, typeName + Path.GetExtension(path));
                AssetDatabase.MoveAsset(path, newPath);
                path = newPath;

            }

            editor.serializedObject.ApplyModifiedProperties();
            GUI.backgroundColor = oldColor;
            GUI.enabled = true;
        }
    }
#endif
}
