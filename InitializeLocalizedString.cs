﻿using PropertyAttribute = UnityEngine.PropertyAttribute;

namespace SMDev.Unitilities
{
    public class InitializeLocalizedString : PropertyAttribute
    {
        public string TableName { get; set; }
        public string Key { get; set; }
        public (string text, string locale)[] Entries { get; set; }

        public InitializeLocalizedString(string tableName, string key = "item_{name}_name", string text = "{name}", string locale = "en")
        {
            TableName = tableName;
            Key = key;
            Entries = new[]
            {
                (text, locale)
            };
        }

        public InitializeLocalizedString(string tableName, string key, params (string text, string locale)[] entries)
        {
            TableName = tableName;
            Key = key;
            Entries = entries;
        }
    }
}