﻿using System;
using UnityEngine;

namespace SMDev.Unitilities
{
    public abstract class InstancedScriptableObject<T> : ScriptableObject
    {
        protected abstract T CreateInstance();

        [NonSerialized]
        T instance;

        public T Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = CreateInstance();
                }
                return instance;
            }
        }
    }

    public abstract class RuntimeInstantiateScriptableObject<SelfReferenceType> : InstancedScriptableObject<SelfReferenceType> where SelfReferenceType : RuntimeInstantiateScriptableObject<SelfReferenceType>
    {
        protected override SelfReferenceType CreateInstance()
        {
            return Instantiate(this) as SelfReferenceType;
        }
    }

    public abstract class PlayModeRestoringScriptableObject : ScriptableObject
    {
        [NonSerialized]
        string serialized;

        [NonSerialized]
        bool restoreOnExitPlayMode = true;

        [ContextMenu("Set Restoration Point", true)]
        bool ValidateSave() => Application.isPlaying;

        [ContextMenu("Set Restoration Point")]
        void Save()
        {
            serialized = JsonUtility.ToJson(this);
        }

        [ContextMenu("Restore Off", true)]
        bool RestoreOffValidate() => restoreOnExitPlayMode;


        [ContextMenu("Restore Off")]
        void RestoreOff()
        {
            restoreOnExitPlayMode = false;
        }


        [ContextMenu("Restore On", true)]
        bool RestoreOnValidate() => restoreOnExitPlayMode == false;
        [ContextMenu("Restore On")]
        void RestoreOn()
        {
            restoreOnExitPlayMode = true;
        }


#if UNITY_EDITOR
        private void EditorApplication_playModeStateChanged(UnityEditor.PlayModeStateChange obj)
        {
            if (obj == UnityEditor.PlayModeStateChange.EnteredPlayMode)
            {
                Save();
                //Debug.Log("Saved in EnterPlay " + name);
            }
            else if (obj == UnityEditor.PlayModeStateChange.ExitingPlayMode)
            {
                if (restoreOnExitPlayMode)
                {
                    JsonUtility.FromJsonOverwrite(serialized, this);
                    //Debug.Log("Restore in ExitPlay " + name);
                }
                else
                {
                    restoreOnExitPlayMode = true;
                }
            }
        }
#endif
        protected virtual void OnEnable()
        {
#if UNITY_EDITOR
            if (Application.isPlaying)
            {
                Save();
                //Debug.Log("Saved in OnEnable " + name);
            }

            UnityEditor.EditorApplication.playModeStateChanged += EditorApplication_playModeStateChanged;
            //Debug.Log("Added listener " + name);
#endif
        }

        protected virtual void OnDisable()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.playModeStateChanged -= EditorApplication_playModeStateChanged;
            //Debug.Log("Removed listener " + name);
#endif
        }
    }
}
