﻿using UnityEngine;

namespace SMDev.Unitilities
{
    public static class Ballistics
    {
        public static bool CalculateFireVector(Vector3 start, Vector3 target, float startingVelocity, Vector3 constantAcceleration, out Vector3 fireVector, out Vector3 highFireVector)
        {
            Vector3 dir = target - start;
            if (constantAcceleration == Vector3.zero)
            {
                highFireVector = fireVector = dir.normalized;
                return true;
            }
            var gravityRotation = Quaternion.FromToRotation(constantAcceleration, Vector3.down);

            dir = gravityRotation * dir;

            float vSqr = startingVelocity * startingVelocity;
            float y = dir.y;
            dir.y = 0.0f;
            float xSqr = dir.sqrMagnitude;
            float g = constantAcceleration.magnitude;

            float uRoot = vSqr * vSqr - g * (g * (xSqr) + (2.0f * y * vSqr));


            float angle;
            float highAngle;
            bool angleFound;
            if (uRoot < 0.0f)
            {
                //target out of range.
                angle = -45.0f;
                highAngle = -45.0f;
                angleFound = false;
            }
            else
            {
                float r = Mathf.Sqrt(uRoot);

                var gx = g * Mathf.Sqrt(xSqr);

                angle = -Mathf.Atan2((vSqr - r), gx) * Mathf.Rad2Deg;
                highAngle = -Mathf.Atan2((vSqr + r), gx) * Mathf.Rad2Deg;
                angleFound = true;
            }

            dir = Quaternion.Inverse(gravityRotation) * dir.normalized;

            var rotationAxis = Vector3.Cross(dir, constantAcceleration);

            fireVector = Quaternion.AngleAxis(angle, rotationAxis) * dir;
            highFireVector = Quaternion.AngleAxis(highAngle, rotationAxis) * dir;
            return angleFound;
        }
    }
}
