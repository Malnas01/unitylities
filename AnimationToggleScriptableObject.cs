﻿using UnityEngine;

namespace SMDev.Unitilities
{
    [CreateAssetMenu(menuName = "Animation Definition/Animation Toggle Scriptable Object")]
    public class AnimationToggleScriptableObject : ScriptableObject
    {
        [SerializeReference, ChooseableSubtype]
        public AnimationToggle Settings;
    }
}