﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UnityEngine;

namespace SMDev.Unitilities
{
    public static class GizmoQueueRaycastExtensions
    {

        public static void RaycastHit(this GizmoQueue gizmoQueue, RaycastHit raycastHit, Ray ray, Color color, float time = 0f)
        {
            gizmoQueue.DrawLine(ray.origin, raycastHit.point, color, time);
        }

        public static void RaycastHit(this GizmoQueue gizmoQueue, RaycastHit raycastHit, Vector3 origin, Color color, float time = 0f)
        {
            gizmoQueue.DrawLine(origin, raycastHit.point, color, time);
        }
        public static void RaycastMiss(this GizmoQueue gizmoQueue, Ray ray, float distance, Color color, float time = 0f)
        {
            gizmoQueue.DrawLine(ray.origin, ray.GetPoint(distance), color, time);
        }

        public static void RaycastMiss(this GizmoQueue gizmoQueue, Vector3 origin, Vector3 direction, float distance, Color color, float time = 0f)
        {
            gizmoQueue.DrawLine(origin, origin + direction.normalized * distance, color, time);
        }
    }

    public class GizmoQueue
    {
        public GameObject RequiredSelected;
#if UNITY_EDITOR
        Dictionary<Color, List<LineInfo>> lines = new Dictionary<Color, List<LineInfo>>();
        Dictionary<Color, List<SphereInfo>> wireSpheres = new Dictionary<Color, List<SphereInfo>>();
        Dictionary<Color, List<BoxInfo>> wireBoxes = new Dictionary<Color, List<BoxInfo>>();
        List<LabelInfo> labels = new();

        bool ShouldRun()
        {
            if(RequiredSelected == null)
            {
                return true;
            }
            return UnityEditor.Selection.gameObjects.Contains(RequiredSelected);
        }

#endif

        public GizmoQueue(GameObject requiresSelected = null)
        {
            RequiredSelected = requiresSelected;
        }

        [Conditional("UNITY_EDITOR")]
        public void DrawLabel(Vector3 position, GUIContent content, GUIStyle style = null, float time = 0)
        {
#if UNITY_EDITOR
            if (!ShouldRun()) return;
            labels.Add(new LabelInfo(position, content, style, Time.time + time));
#endif
        }

        [Conditional("UNITY_EDITOR")]
        public void DrawLine(Vector3 start, Vector3 end, Color color, float time = 0)
        {
#if UNITY_EDITOR
            if (!ShouldRun()) return;
            if (!lines.ContainsKey(color))
            {
                lines.Add(color, new List<LineInfo>());
            }
            lines[color].Add((start, end, Time.time + time));
#endif
        }

        [Conditional("UNITY_EDITOR")]
        public void DrawRay(Ray ray, float distance, Color color, float time = 0)
        {
#if UNITY_EDITOR
            if (!ShouldRun()) return;
            if (!lines.ContainsKey(color))
            {
                lines.Add(color, new List<LineInfo>());
            }
            lines[color].Add((ray.origin, ray.origin + ray.direction * distance, Time.time + time));
#endif
        }

        [Conditional("UNITY_EDITOR")]
        public void DrawWireSphere(Vector3 origin, float radius, Color color, float time = 0)
        {
#if UNITY_EDITOR
            if (!ShouldRun()) return;
            if (!wireSpheres.ContainsKey(color))
            {
                wireSpheres.Add(color, new List<SphereInfo>());
            }
            wireSpheres[color].Add((origin, radius, Time.time + time));
#endif
        }

        [Conditional("UNITY_EDITOR")]
        public void DrawWireCube(Vector3 origin, Vector3 size, Color color, float time = 0)
        {
#if UNITY_EDITOR
            if (!ShouldRun()) return;
            if (!wireBoxes.ContainsKey(color))
            {
                wireBoxes.Add(color, new List<BoxInfo>());
            }
            wireBoxes[color].Add((origin, size, Time.time + time));
#endif
        }

        [Conditional("UNITY_EDITOR")]
        public void Execute(bool clear = true)
        {
#if UNITY_EDITOR
            foreach (var kvp in lines)
            {
                Gizmos.color = kvp.Key;
                foreach (var line in kvp.Value)
                {
                    Gizmos.DrawLine(line.start, line.end);
                }
            }

            foreach (var kvp in wireSpheres)
            {
                Gizmos.color = kvp.Key;
                foreach (var sphere in kvp.Value)
                {
                    Gizmos.DrawWireSphere(sphere.origin, sphere.radius);
                }
            }

            foreach (var kvp in wireBoxes)
            {
                Gizmos.color = kvp.Key;
                foreach (var box in kvp.Value)
                {
                    Gizmos.DrawWireCube(box.origin, box.size);
                }
            }

            foreach(var handle in labels)
            {
                UnityEditor.Handles.Label(handle.position, handle.content, handle.style);
            }

            if (clear) Clear();
#endif
        }

        [Conditional("UNITY_EDITOR")]
        public void Clear()
        {
#if UNITY_EDITOR            
            foreach(var kvp in lines)
            {
                for(int i = kvp.Value.Count - 1; i >= 0; i--)
                {
                    if (kvp.Value[i].timeToRemove < Time.time)
                    {
                        kvp.Value.RemoveAt(i);
                    }
                }
            }
            foreach (var kvp in wireSpheres)
            {
                for (int i = kvp.Value.Count - 1; i >= 0; i--)
                {
                    if (kvp.Value[i].timeToRemove < Time.time)
                    {
                        kvp.Value.RemoveAt(i);
                    }
                }
            }
            foreach (var kvp in wireBoxes)
            {
                for (int i = kvp.Value.Count - 1; i >= 0; i--)
                {
                    if (kvp.Value[i].timeToRemove < Time.time)
                    {
                        kvp.Value.RemoveAt(i);
                    }
                }
            }

            for (int i = labels.Count - 1; i >= 0; i--)
            {
                if (labels[i].timeToRemove < Time.time)
                {
                    labels.RemoveAt(i);
                }
            }
#endif
        }
        [Conditional("UNITY_EDITOR")]
        public void ForceClearAll()
        {
#if UNITY_EDITOR
            lines.Clear();
            wireSpheres.Clear();
            wireBoxes.Clear();
            labels.Clear();
#endif
        }
    }

    internal struct LineInfo
    {
        public Vector3 start;
        public Vector3 end;
        public float timeToRemove;

        public LineInfo(Vector3 start, Vector3 end, float time)
        {
            this.start = start;
            this.end = end;
            this.timeToRemove = time;
        }

        public override bool Equals(object obj)
        {
            return obj is LineInfo other &&
                   start.Equals(other.start) &&
                   end.Equals(other.end) &&
                   timeToRemove == other.timeToRemove;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(start, end, timeToRemove);
        }

        public void Deconstruct(out Vector3 start, out Vector3 end, out float time)
        {
            start = this.start;
            end = this.end;
            time = this.timeToRemove;
        }

        public static implicit operator (Vector3 start, Vector3 end, float time)(LineInfo value)
        {
            return (value.start, value.end, value.timeToRemove);
        }

        public static implicit operator LineInfo((Vector3 start, Vector3 end, float time) value)
        {
            return new LineInfo(value.start, value.end, value.time);
        }
    }

    internal struct SphereInfo
    {
        public Vector3 origin;
        public float radius;
        public float timeToRemove;

        public SphereInfo(Vector3 origin, float radius, float time)
        {
            this.origin = origin;
            this.radius = radius;
            this.timeToRemove = time;
        }

        public override bool Equals(object obj)
        {
            return obj is SphereInfo other &&
                   origin.Equals(other.origin) &&
                   radius == other.radius &&
                   timeToRemove == other.timeToRemove;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(origin, radius, timeToRemove);
        }

        public void Deconstruct(out Vector3 origin, out float radius, out float time)
        {
            origin = this.origin;
            radius = this.radius;
            time = this.timeToRemove;
        }

        public static implicit operator (Vector3 origin, float radius, float time)(SphereInfo value)
        {
            return (value.origin, value.radius, value.timeToRemove);
        }

        public static implicit operator SphereInfo((Vector3 origin, float radius, float time) value)
        {
            return new SphereInfo(value.origin, value.radius, value.time);
        }
    }

    internal struct LabelInfo
    {
        public Vector3 position;
        public GUIContent content;
        public GUIStyle style;
        public float timeToRemove;

        public LabelInfo(Vector3 origin, GUIContent content, GUIStyle style, float timeToRemove)
        {
            this.position = origin;
            this.content = content ?? new GUIContent();
            this.style = style ?? new GUIStyle();
            this.timeToRemove = timeToRemove;
        }
    }

    internal struct BoxInfo
    {
        public Vector3 origin;
        public Vector3 size;
        public float timeToRemove;

        public BoxInfo(Vector3 origin, Vector3 radius, float time)
        {
            this.origin = origin;
            this.size = radius;
            this.timeToRemove = time;
        }

        public override bool Equals(object obj)
        {
            return obj is BoxInfo other &&
                   origin.Equals(other.origin) &&
                   size == other.size &&
                   timeToRemove == other.timeToRemove;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(origin, size, timeToRemove);
        }

        public void Deconstruct(out Vector3 origin, out Vector3 size, out float time)
        {
            origin = this.origin;
            size = this.size;
            time = this.timeToRemove;
        }

        public static implicit operator (Vector3 origin, Vector3 size, float time)(BoxInfo value)
        {
            return (value.origin, value.size, value.timeToRemove);
        }

        public static implicit operator BoxInfo((Vector3 origin, Vector3 size, float time) value)
        {
            return new BoxInfo(value.origin, value.size, value.time);
        }
    }
}
