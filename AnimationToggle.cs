﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace SMDev.Unitilities
{
    [System.Serializable]
    public abstract class AnimationToggle
    {
        public abstract void SetOn(Animator animator, bool value);
        public abstract bool IsOn(Animator animator);
    }
    public static class AnimationToggleExtensions
    {
        public static void TrySetOn(this AnimationToggle player, Animator animator, bool value)
        {
            if (animator != null && player != null)
            {
                player.SetOn(animator, value);
            }
        }

        public static bool TryIsOn(this AnimationToggle player, Animator animator)
        {
            if (animator != null && player != null)
            {
                return player.IsOn(animator);
            }
            return false;
        }
    }


    public class AnimationToggleBoolParameter : AnimationToggle
    {
        public string boolParameterName;
        public bool onValue = true;

        public override bool IsOn(Animator animator)
        {
            return animator.GetBool(boolParameterName) == onValue;
        }

        public override void SetOn(Animator animator, bool value)
        {
            animator.SetBool(boolParameterName, onValue == value);
        }
    }

    public class AnimationTogglePlayState : AnimationToggle
    {
        public int layer = -1;
        public string animatorPlayState;

        public string animatorStopState;

        public override bool IsOn(Animator animator)
        {
            if (layer < 0)
            {
                for (int i = 0; i < animator.layerCount; i++)
                {
                    if (animator.GetCurrentAnimatorStateInfo(layer).IsName(animatorPlayState)) 
                    {
                        return true;
                    }
                }
                return false;
            }
            return animator.GetCurrentAnimatorStateInfo(layer).IsName(animatorPlayState);
        }

        public override void SetOn(Animator animator, bool value)
        {
            if (value)
            {
                if(!string.IsNullOrEmpty(animatorPlayState))
                {

                    animator.Play(animatorPlayState, layer);
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(animatorStopState))
                {
                    animator.Play(animatorStopState, layer);
                }
            }
        }
    }

    public class AnimationToggleSO : AnimationToggle
    {
        [EditInline]
        public AnimationToggleScriptableObject ScriptableObject;

        public override bool IsOn(Animator animator)
        {
            if(ScriptableObject == null)
            {
                return false;
            }
            return ScriptableObject.Settings.IsOn(animator);
        }

        public override void SetOn(Animator animator, bool value)
        {
            if (ScriptableObject != null)
            {
                ScriptableObject.Settings.SetOn(animator, value);
            }
        }
    }

    public class MultiToggle : AnimationToggle
    {
        [SerializeReference, ChooseableSubtype]
        public List<AnimationToggle> triggers;

        public override bool IsOn(Animator animator)
        {
            return triggers.Any(t => t.IsOn(animator));
        }

        public override void SetOn(Animator animator, bool value)
        {
            foreach (var t in triggers)
            {
                t.SetOn(animator, value);
            }
        }
    }
}