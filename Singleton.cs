﻿using System;
using System.Reflection;
using UnityEngine;
using Debug = UnityEngine.Debug;


namespace SMDev.Unitilities
{

    /// <summary>
    /// A class with this attribute will create an instance at runtime when calling <see cref="Singleton{T}.Instance"/> if no instance already exists. It can still be used on classes that dont extend <see cref="Singleton{T}"/>
    /// </summary>
    public class SingletonCanCreateAtRuntimeAttribute : System.Attribute { }
    /// <summary>
    /// A <see cref="Singleton{T}"/> with this attribute will not automatically destroy itself if other instances already exist.
    /// </summary>
    public class SingletonCanHaveMultipleInstancesAttribute : System.Attribute { }

    /// <summary>
    /// A <see cref="Singleton{T}"/> with this attribute will not <see cref="Object.DontDestroyOnLoad(Object)"/>
    /// </summary>
    public class SingletonDontDestroyOnLoadAttribute : System.Attribute { }

    /// A <see cref="Singleton{T}"/> with this attribute will look for a prefab at Resources/Singleton Prefabs/[ClassName]"/>
    /// </summary>
    public class SingletonCreateFromPrefabAttribute : System.Attribute 
    { 
        public string ResourcePathOverride { get; set; }
    }

    /// <summary>
    /// This class can be extended to easily create singletons. 
    /// <see cref="Singleton{T}.Instance"/> can also be called on classes that dont extend <see cref="Singleton{T}"/> to return instances that have been set using <see cref="Singleton{T}.SetInstance(T)"/>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class Singleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.SubsystemRegistration)]
        static void ResetStaticValues()
        {
            instance = null;
        }

        static T instance;

        //static bool quitting = false;

        /// <summary>
        /// Return an instance of class <see cref="{T}"/>
        /// </summary>
        public static T Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<T>();
                    if (instance == null && !ApplicationIsQuitting.IsQuitting)
                    {
                        if (typeof(T).GetTypeInfo().IsDefined(typeof(SingletonCanCreateAtRuntimeAttribute)) && Application.isPlaying)
                        {
                            Debug.Log("Can't find singleton " + typeof(T).ToString() + ", creating...");
                            instance = CreateInstance();
                        }
                    }
                }
                //Return true null in case instance is a destroyed game object
                return instance == null ? null : instance;
            }
        }

        protected static T CreateInstance()
        {
            var createFromPrefabAttribute = typeof(T).GetTypeInfo().GetCustomAttribute<SingletonCreateFromPrefabAttribute>();
            if (createFromPrefabAttribute != null)
            {
                var path = string.IsNullOrEmpty(createFromPrefabAttribute.ResourcePathOverride) ? $"Singleton Prefabs/{typeof(T)}" : createFromPrefabAttribute.ResourcePathOverride;
                var prefab = Resources.Load<T>(path);
                if (prefab != null)
                {
                    return UnityEngine.Object.Instantiate(prefab);
                }
                Debug.LogWarning($"Tried to create singleton {typeof(T)} from prefab but could not find prefab at {path}");
            }
            return new GameObject(typeof(T).ToString()).AddComponent<T>();
        }

        /// <summary>
        /// Set the singleton instance to this object
        /// </summary>
        /// <param name="me">The object instance to set {T}.Instance to</param>
        public static bool SetInstance(T me)
        {
            if (instance == null)
            {
                if (Application.isPlaying && typeof(T).GetTypeInfo().IsDefined(typeof(SingletonDontDestroyOnLoadAttribute)))
                    DontDestroyOnLoad(me.gameObject);
                instance = me;
                return true;
            }
            else if (instance != me && !typeof(T).GetTypeInfo().IsDefined(typeof(SingletonCanHaveMultipleInstancesAttribute)))
            {
                Debug.Log("Dupilcate singleton " + typeof(T).ToString() + ", destroying...");
                Destroy(me);
            }
            return false;
        }
        //[RuntimeInitializeOnLoadMethod]
        //static void RunOnStart()
        //{
        //    quitting = false;
        //    Application.quitting -= Application_quitting;
        //}
        protected virtual void Awake()
        {
            //Application.quitting += Application_quitting;
            SetInstance(this as T);
        }

        //private static void Application_quitting()
        //{
        //    quitting = true;
        //}

        protected virtual void OnDestroy()
        {
            //Application.quitting -= Application_quitting;
            if (instance == this)
            {
                instance = null;
            }
        }
    }

    public static class ApplicationIsQuitting
    {
        public static bool IsQuitting { get; set; }

        [RuntimeInitializeOnLoadMethod]
        static void RunOnStart()
        {
            IsQuitting = false;
            Application.quitting += Application_quitting;
        }

        private static void Application_quitting()
        {
            IsQuitting = true;
            Application.quitting -= Application_quitting;
        }
    }


#if UNITY_EDITOR
    [UnityEditor.InitializeOnLoad]
    public static class PlayModeStateChanged
    {
        // register an event handler when the class is initialized
        static PlayModeStateChanged()
        {
            UnityEditor.EditorApplication.playModeStateChanged += PlayModeStateChangedCallback;
        }

        private static void PlayModeStateChangedCallback(UnityEditor.PlayModeStateChange state)
        {
            if(state == UnityEditor.PlayModeStateChange.EnteredEditMode)
            {
                ApplicationIsQuitting.IsQuitting = false;
            }

        }
    }
#endif
}
